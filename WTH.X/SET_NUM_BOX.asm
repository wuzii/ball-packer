;*****************************************************************************
; SET_NUM_BOX
; PIC18F4620
; Clock Speed: 40 MHz
;****************************************************************************
ERRORLEVEL -207,-205
#include <p18f4620.inc>
    list P=18F4620, F=INHX32, C=160, N=80, ST=OFF, MM=OFF, R=DEC
#INCLUDE <LCDKEYPAD.inc>
#INCLUDE <MATH.inc>

;*******************CONSTANT DEFINES***************************************
#DEFINE     KEY1    B'00000000'
#DEFINE     KEY2    B'00000001'
#DEFINE     KEY3    B'00000010'
#DEFINE     KEYA    B'00000011'
#DEFINE     KEY4    B'00000100'
#DEFINE     KEY5    B'00000101'
#DEFINE     KEY6    B'00000110'
#DEFINE     KEYB    B'00000111'
#DEFINE     KEY7    B'00001000'
#DEFINE     KEY8    B'00001001'
#DEFINE     KEY9    B'00001010'
#DEFINE     KEYC    B'00001011'
#DEFINE     KEYS    B'00001100'
#DEFINE     KEY0    B'00001101'
#DEFINE     KEYP    B'00001110'
#DEFINE     KEYD    B'00001111'

TEMP_VAR    EQU     0X28        ;BUFFER
TEMP_VAR2   EQU     0X29        ;BUFFER
NUM_BOX0    EQU     0X2A        ;STORE UNIT DIGIT OF NUMBER OF BOXES, BIT0:3 FOR NUMBER, BIT4 FOR SET/NOT SET(1/0)
NUM_BOX1    EQU     0X2B        ;STORE TENTH DIGIT OF NUMBER OF BOXES
NUM_BOX     EQU     0X2C        ;STORE NUMBER OF BOXES(NUM_BOX1*10+NUM_BOX)

code
	global SET_NUM_BOX
;Only these functions are visible to other asm files.

MSG3
        DB              "Enter the number", 0
MSG4
        DB              "of boxes=", 0


;*************************************************************
; THE CODE FOR SETTING NUM_BOX
;*************************************************************
SET_NUM_BOX
        MOVLW           B'00000000'     ;INITIALIZE NUM_BOX TO 0
        MOVWF           NUM_BOX0
        MOVWF           NUM_BOX1
        CALL            CLEAR_LCD
        DISPLAY_TABLE   MSG3
        CALL            SWITCH_LINE
        DISPLAY_TABLE   MSG4
        SHOW_CURSOR
READ_BOX_NUMBER
        CALL            POLL_KEY
        MOVWF           TEMP_VAR        ;STORE KEY HEX IN TEMP_VAR
;CHECK_CANCEL_KEY
        MOVLW           KEYC
        CPFSEQ          TEMP_VAR, W     ; CHECK IF IS C KEY
        BRA             CHECK_ENTER_KEY
        BRA             SET_NUM_BOX
CHECK_ENTER_KEY
        MOVLW           KEYD
        CPFSEQ          TEMP_VAR, W     ; CHECK IF IS D KEY
        BRA             CHECK_LETTER_KEY
        BRA             IS_D_KEY
CHECK_LETTER_KEY
        MOVLW           B'00000011'
        MOVWF           TEMP_VAR2       ;STORE 00000011 IN TEMP_VAR2
        ANDWF           TEMP_VAR, W     ;STORE MASKED RESULT IN W
        CPFSEQ          TEMP_VAR2       ;IF TRUE THEN IS KEY A,B,C,D
        BRA             CHECK_STAR_KEY
        BRA             READ_BOX_NUMBER
CHECK_STAR_KEY
        MOVLW           KEYS
        CPFSEQ          TEMP_VAR, W     ; CHECK IF IS STAR KEY
        BRA             CHECK_POUND_KEY
        BRA             READ_BOX_NUMBER
CHECK_POUND_KEY
        MOVLW           KEYP
        CPFSEQ          TEMP_VAR, W     ; CHECK IF IS POUND KEY
        BRA             IS_NUMBER_KEY
        BRA             READ_BOX_NUMBER
IS_NUMBER_KEY
        BTFSC           NUM_BOX0, 4     ; TEST BIT4 OF NUM_BOX0
        BRA             TRY_TENTH       ; IF SET
        MOVF            TEMP_VAR, W     ; MOVE ORIGINAL NUMBER BACK TO W
        CALL            KPHexToNum      ; CONVET KP HEX TO THEIR VALUE
        MOVWF           NUM_BOX0        ; STORE UNIT DIGIT IN NUM_BOX0<0:3>
        BSF             NUM_BOX0, 4     ; SET BIT4
        BRA             DISPLAY_A_DIGIT
TRY_TENTH
        BTFSC           NUM_BOX1, 4     ; TEST BIT4 OF NUM_BOX1
        BRA             OVER_FLOW       ; IF SET
        MOVF            NUM_BOX0, W     ; MOVE THE UNIT DIGIT TO TENTH DIGIT
        MOVWF           NUM_BOX1
        MOVF            TEMP_VAR, W     ; MOVE ORIGINAL NUMBER BACK TO W
        CALL            KPHexToNum      ; CONVET KP HEX TO THEIR VALUE
        MOVWF           NUM_BOX0        ; STORE UNIT DIGIT IN NUM_BOX0<0:3>
        BSF             NUM_BOX0, 4     ; SET BIT4
DISPLAY_A_DIGIT
        MOVF            TEMP_VAR, W     ; MOVE ORIGINAL NUMBER BACK TO W
        CALL            KPHexToChar     ; DISPLAY THE NUMBER
        CALL            WR_DATA
        BRA             READ_BOX_NUMBER
OVER_FLOW
        BRA             READ_BOX_NUMBER     ; WAIT FOR THE D KEY
IS_D_KEY
        BTFSS           NUM_BOX0, 4             ; TEST BIT4 OF NUM_BOX0
        BRA             READ_BOX_NUMBER     ; WAIT FOR A NUMBER TO BE INPUT
STORE_NUM_BOX
        COMBINE_DIGITS  NUM_BOX0, NUM_BOX1
        MOVWF           NUM_BOX
        RETURN
END