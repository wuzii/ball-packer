EXTERN START_BELT_DC, STOP_BELT_DC, LOWER_LATCH_MOTOR, LIFT_LATCH_MOTOR, STOP_LATCH_MOTOR

;*************************************************************
; ROTATE UPPER DISK TO THE SPECIFIED POSITION
; [POS_LIT]: 0 -> ROTATE TO CENTER, 1 -> ROTATE TO THE RESERVIOR
;               SPECIFIED BY CURRENT_COLOR
; W WILL BE MODIFIED!
;*************************************************************
ROTATE_UPPER MACRO POS_LIT
local   CHECK_COLOR, WHITE, RED, CENTRE, DONE
        MOVLW       POS_LIT
        TSTFSZ      WREG
        BRA         CHECK_COLOR
        BRA         CENTRE      ;IF ZERO, ROTATE TO CENTER
    CHECK_COLOR       
        TSTFSZ      CURRENT_COLOR       ;CHECK CURRENT COLOR
        BRA         RED                 ;IF NOT ZERO, ROTATE TO RED RESERV
    WHITE
        MOVLW       UPPER_POS_WHITE
        MOVWF       UPPER_SERVO_MARK
        BRA         DONE
    RED
        MOVLW       UPPER_POS_RED
        MOVWF       UPPER_SERVO_MARK
        BRA         DONE
    CENTRE
        MOVLW       UPPER_POS_CENTER
        MOVWF       UPPER_SERVO_MARK
        BRA         DONE
    DONE
ENDM

;*************************************************************
; ROTATE LOWER DISK TO THE POSITION SPECIFIED BY BALL_COUNTER
; W WILL BE MODIFIED!
;*************************************************************
ROTATE_LOWER MACRO
        LOCAL POS_1, POS_2, POS_3, POS_4, POS_5, POSITION_0, POSITION_1, POSITION_2, POSITION_3, POSITION_4, POSITION_5, DONE
        MOVF        BALL_COUNTER, W     ; IF BALL_COUNTER ZERO, THEN ROTATE TO POSITION0
        DECF        WREG, W
        TSTFSZ      WREG
        BRA         POS_1
        BRA         POSITION_0
POS_1
        DECF        WREG
        TSTFSZ      WREG
        BRA         POS_2
        BRA         POSITION_1
POS_2
        DECF        WREG
        TSTFSZ      WREG
        BRA         POS_3
        BRA         POSITION_2
POS_3
        DECF        WREG
        TSTFSZ      WREG
        BRA         POS_4
        BRA         POSITION_3
POS_4
        DECF        WREG
        TSTFSZ      WREG
        BRA         POS_5
        BRA         POSITION_4
POS_5
        DECF        WREG
        TSTFSZ      WREG
        BRA         DONE
        BRA         POSITION_5
    POSITION_0
        MOVLW           LOWER_POS_0
        MOVWF           LOWER_SERVO_MARK
        BRA         DONE
    POSITION_1
        MOVLW           LOWER_POS_1
        MOVWF           LOWER_SERVO_MARK
        BRA         DONE
    POSITION_2
        MOVLW           LOWER_POS_2
        MOVWF           LOWER_SERVO_MARK
        BRA         DONE
    POSITION_3
        MOVLW           LOWER_POS_3
        MOVWF           LOWER_SERVO_MARK
        BRA         DONE
    POSITION_4
        MOVLW           LOWER_POS_4
        MOVWF           LOWER_SERVO_MARK
        BRA         DONE
    POSITION_5
        MOVLW           LOWER_POS_5
        MOVWF           LOWER_SERVO_MARK
        BRA         DONE
    DONE
ENDM