
;*************************************************************
; POLL A SENDOR UNTILL IT IS SET
; [PORT_REG]: THE I/O PORT
; [BIT_LIT]: THE BIT
;*************************************************************
POLL_SENSOR_SET MACRO PORT_REG, BIT_LIT
            LOCAL POLL
    POLL    BTFSS   PORT_REG, BIT_LIT         ;Wait until BIT IS SET
            BRA     POLL
ENDM

;*************************************************************
; POLL A SENDOR UNTILL IT IS CLEAR
; [PORT_REG]: THE I/O PORT
; [BIT_LIT]: THE BIT
;*************************************************************
POLL_SENSOR_CLEAR MACRO PORT_REG, BIT_LIT
            LOCAL POLL
    POLL    BTFSC   PORT_REG, BIT_LIT         ;Wait until BIT IS CLEAR
            BRA     POLL
ENDM