;*****************************************************************************
; MAIN
; PIC18F4620
; Clock Speed: 10 MHz
;****************************************************************************
ERRORLEVEL -207,-205
#include <p18f4620.inc>
    list P=18F4620, F=INHX32, C=160, N=80, ST=OFF, MM=OFF, R=DEC
#INCLUDE <PERM_LOG.inc>
;*******************CONSTANT DEFINATIONS***************************************
#DEFINE     KEY1    B'00000000'
#DEFINE     KEY2    B'00000001'
#DEFINE     KEY3    B'00000010'
#DEFINE     KEYA    B'00000011'
#DEFINE     KEY4    B'00000100'
#DEFINE     KEY5    B'00000101'
#DEFINE     KEY6    B'00000110'
#DEFINE     KEYB    B'00000111'
#DEFINE     KEY7    B'00001000'
#DEFINE     KEY8    B'00001001'
#DEFINE     KEY9    B'00001010'
#DEFINE     KEYC    B'00001011'
#DEFINE     KEYS    B'00001100'
#DEFINE     KEY0    B'00001101'
#DEFINE     KEYP    B'00001110'
#DEFINE     KEYD    B'00001111'

#DEFINE     CENTER  B'00000000'
#DEFINE     SIDE    B'00000001'

;*************** Sensors************************
#DEFINE     BOX_POS_SENSOR      PORTC,5
#DEFINE     CENTER_PIPE_SENSOR  PORTC,6
#DEFINE     BOTTOM_PIPE_SENSOR  PORTC,7
#DEFINE     LATCH_SENSOR_LOWER  PORTB,6
#DEFINE     LATCH_SENSOR_UPPER  PORTB,7
#DEFINE     EM_STOP_SWITCH      PORTB,0
;*********************************************

;************ DC *****************************
#DEFINE     LATCH_DC_HIGH        LATC,1         ; LATCH DC HIGHER BIT
#DEFINE     LATCH_DC_LOW         LATC,0         ; LATCH DC LOWER BIT
#DEFINE     BELT_DC              LATC,2
;*********************************************


;**********  Servo  OCS FREQ=40MHz, TMR0 PRESCALER=4
#DEFINE     PWM_PERIOD          B'11001000'             ;200*0.1ms=20ms
#DEFINE     TIMER0_RESET        B'00000110'     ;6, TMR0 COUNT FROM 6-255
#DEFINE     UPPER_SERVO         LATE,0
#DEFINE     LOWER_SERVO         LATE,1
#DEFINE     SERVO_CENTER_MARK   B'10111001'     ;185
#DEFINE     UPPER_POS_WHITE     B'11000001'     ;193
#DEFINE     UPPER_POS_RED       B'10110001'     ;177
#DEFINE     UPPER_POS_CENTER    B'10111010'     ;186

#DEFINE     LOWER_POS_0         B'10110100'     ;180
#DEFINE     LOWER_POS_1         B'10110110'     ;182
#DEFINE     LOWER_POS_2         B'10111000'     ;184
#DEFINE     LOWER_POS_3         B'10111010'     ;186
#DEFINE     LOWER_POS_4         B'10111101'     ;189
#DEFINE     LOWER_POS_5         B'10111111'     ;191
;***************

TEMP_LCD    EQU     0x20        ;BUFFER FOR LCD INSTRUCTION
DAT         EQU     0X21        ;BUFFER FOR LCD DATA
DELAY1      EQU     0X25
DELAY2      EQU     0X26
TEMP_CHAR   EQU     0X27        ;BUFFER FOR KEY PAD CHAR

TEMP_VAR    EQU     0X28        ;BUFFER
TEMP_VAR2   EQU     0X29        ;BUFFER
NUM_BOX0    EQU     0X2A        ;STORE UNIT DIGIT OF NUMBER OF BOXES, BIT0:3 FOR NUMBER, BIT4 FOR SET/NOT SET(1/0)
NUM_BOX1    EQU     0X2B        ;STORE TENTH DIGIT OF NUMBER OF BOXES
NUM_BOX     EQU     0X2C        ;STORE NUMBER OF BOXES(NUM_BOX1*10+NUM_BOX)
TEMP_VAR3   EQU     0X2D        ;BUFFER
TEMP_VAR4   EQU     0X2E        ;BUFFER

BOX_COUNTER     EQU     0X2F    ;COUNT THE BOX
BALL_COUNTER    EQU     0X30    ;COUNT THE BALL (VALUE FROM 1 -6)
CURRENT_COLOR   EQU     0X31    ;CURRENT COLOR OF THE BALL TO DISPENSE (0/1)
CURRENT_PATTERN EQU     0X32



W_RESERV_COUNTER    EQU 0X33    ;COUNT THE NUMBER OF WHITE BALLS IN RESERVOIR
R_RESERV_COUNTER    EQU 0X34    ;COUNT THE NUMBER OF RED BALLS IN RESERVOIR

BALL_PATTERN_MASK   EQU 0X35    ;BIT MASK FOR PATTERN

SERVO_PERIOD_COUNT  EQU 0X36    ;COUNT FROM 0 TO 200 FOR 20MS
UPPER_SERVO_MARK    EQU 0X37    ;MARK DUTY CYCLE LENGTH = (200-THIS)*0.1ms, [180-190]
LOWER_SERVO_MARK    EQU 0X38

WREG_TEMP           EQU 0X39    ;STACK REGS FOR INTERRUPT(LOW)
STATUS_TEMP         EQU 0X3A
BSR_TEMP            EQU 0X3B

WREG_TEMP_H         EQU 0X3C    ;STACK REGS FOR INTERRUPT(HIGH)
STATUS_TEMP_H       EQU 0X3D
BSR_TEMP_H          EQU 0X3E

ENDING_STATUS_PAGE   EQU 0X3F   ; NUMBER OF PAGES IN ENDING STATUS

TIME_DIFFERENCE     EQU 0X40

W_RES_INC           EQU 0X41    ; NUMBER OF BALLS TO BE INCREMENTED (0 OR 1)
R_RES_INC           EQU 0X42

W_RES_DEB_COUNTER   EQU 0X43    ; COUNTERS FOR RESERVOIR DEBOUNCING TIMER
R_RES_DEB_COUNTER   EQU 0X44

TEMP_VAR5           EQU 0X45
TEMP_VAR6           EQU 0X46

PAGE_NUM            EQU 0X47

PATTERN_STACK   EQU     0X100   ;THE PATTERN STACK STARTS AT 0X00 OF "BANK1"

code
	global  

MSG1
        DB              "<IDLE>", 0    ;DECLEAR TABLE
MSG2
        DB              "START:D", 0    ;DECLEAR TABLE

MSG5
        DB              "Box number=", 0
MSG6
        DB              "Pattern  = (1-6)",0
MSG7
        DB              "Press D to start",0
PATTERN
        DB              B'00000000', B'00000000', B'00111111', B'00111000', B'00010010', B'00101101', B'00101010'
MSG8
        DB              "BOX:  /  ",0
MSG9
        DB              "Completed!  1/",0
MSG10
        DB              "For info press A",0
MSG17
        DB              "Calibrating..",0
MSG18
        DB              "START:D",0
MSG19
        DB              "Connect to PC?", 0
MSG20
        DB              "Yes: A   Skip: D", 0

PC_OPTION_SCREEN
        CALL            CLEAR_LCD
        DISPLAY_TABLE   MSG19
        CALL            SWITCH_LINE
        DISPLAY_TABLE   MSG20


