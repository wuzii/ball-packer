;*****************************************************************************
; LCDKEYPAD
; Displays characters to the LCD
; PIC18F4620
; Clock Speed: 40 MHz
;****************************************************************************
ERRORLEVEL -207,-205
#include <p18f4620.inc>
    list P=18F4620, F=INHX32, C=160, N=80, ST=OFF, MM=OFF, R=DEC
#INCLUDE <rtc_macros.inc>

;*******************CONSTANT DEFINES***************************************

#DEFINE     RS      LATD,2
#DEFINE     E       LATD,3
#DEFINE     KP_DA   PORTA,4
#DEFINE     KP_PORT PORTA

TEMP_LCD    EQU     0x20        ;BUFFER FOR LCD INSTRUCTION
DAT         EQU     0X21        ;BUFFER FOR LCD DATA
DELAY1      EQU     0X25
DELAY2      EQU     0X26
TEMP_CHAR   EQU     0X27        ;BUFFER FOR KEY PAD CHAR

TEMP_VAR    EQU     0X28        ;BUFFER
TEMP_VAR2   EQU     0X29        ;BUFFER
NUM_BOX0    EQU     0X2A        ;STORE UNIT DIGIT OF NUMBER OF BOXES, BIT0:3 FOR NUMBER, BIT4 FOR SET/NOT SET(1/0)
NUM_BOX1    EQU     0X2B        ;STORE TENTH DIGIT OF NUMBER OF BOXES
NUM_BOX     EQU     0X2C        ;STORE NUMBER OF BOXES(NUM_BOX1*10+NUM_BOX)
TEMP_VAR3   EQU     0X2D        ;BUFFER
TEMP_VAR4   EQU     0X2E        ;BUFFER

BOX_COUNTER     EQU     0X2F    ;COUNT THE BOX
BALL_COUNTER    EQU     0X30    ;COUNT THE BALL (VALUE FROM 1 -6)
CURRENT_COLOR   EQU     0X31    ;CURRENT COLOR OF THE BALL TO DISPENSE (0/1)
CURRENT_PATTERN EQU     0X32

W_RESERV_COUNTER    EQU 0X33    ;COUNT THE NUMBER OF WHITE BALLS IN RESERVOIR
R_RESERV_COUNTER    EQU 0X34    ;COUNT THE NUMBER OF RED BALLS IN RESERVOIR

BALL_PATTERN_MASK   EQU 0X35    ;BIT MASK FOR PATTERN

PATTERN_STACK   EQU     0X100   ;THE PATTERN STACK STARTS AT 0X00 OF "BANK1"


code
	global INIT_LCD, WR_INS, WR_DATA, SWITCH_LINE, CLEAR_LCD, POLL_KEY, POLL_KEY_WITH_RTC, KPHexToChar, KPHexToNum, BIN_TO_CHAR
;Only these functions are visible to other asm files.

KEY_PAD
        DB      "123A456B789C*0#D"
NUMBER
        DB      "0123456789"
DIGIT
        DB      1,2,3,0,4,5,6,0,7,8,9,0,0,0,0,0
MSG8
        DB              "BOX:  /  ",0

;*************************************************************
; INITIALIZE LCD
; W WILL BE MODIFIED!
;*************************************************************
INIT_LCD
        CLRF    TRISD
        CALL    Delay5ms            ; WAIT FOR LCD TO START UP
        CALL    Delay5ms
        MOVLW   B'00110011'         ; Ensure 8-bit mode first (no way to immediately guarantee 4-bit mode)
        CALL    WR_INS
        MOVLW   B'00110010'         ; -> Send b'0011' 3 times
        CALL    WR_INS
        MOVLW   B'00101000'         ; 4 bits, 2 lines, 5x7 dots
        CALL    WR_INS
        MOVLW   B'00001100'         ; display on/off
        CALL    WR_INS
        MOVLW   B'00000110'         ; Entry mode
        CALL    WR_INS
        MOVLW   B'00000001'         ; Clear ram
        CALL    WR_INS
        RETURN

;************************************************************
; WRITE COMMAND TO LCD
; INPUT: W
; OUTPUT: -
; W WILL BE MODIFIED!
;************************************************************
WR_INS
        BCF     RS              ;clear RS
        MOVWF   TEMP_LCD      ;W ??> com
        ANDLW   0xF0          ;mask 4 bits MSB w = X0
        MOVWF   PORTD         ;Send 4 bits MSB
        BSF     E               ;
        CALL    Delay5ms       ;__    __
        BCF     E               ;  |__|
        SWAPF   TEMP_LCD,w
        ANDLW   0xF0          ;1111 0010
        MOVWF   PORTD         ;send 4 bits LSB
        BSF     E               ;
        CALL    Delay5ms       ;__    __
        BCF     E               ;  |__|
        CALL    Delay5ms
        RETURN

;*************************************************************
; WRITE DATA TO LCD
; INPUT: W
; OUTPUT: -
; W WILL BE MODIFIED!
;*************************************************************
WR_DATA
        BCF     RS
        MOVWF   DAT
        MOVF    DAT,WREG
        ANDLW   0XF0
        ADDLW   4
        MOVWF   PORTD
        BSF     E
        SWAPF   DAT, WREG
        ANDLW   0XF0
        BCF     E
        ADDLW   4
        MOVWF   PORTD
        BSF     E
        NOP
        BCF     E
        CALL    Delay44us
        RETURN

;***************************************************************
; SWITCH LCD TO THE SECOND LINE
; W WILL BE MODIFIED!
;***************************************************************
SWITCH_LINE
        MOVLW   B'11000000'
        CALL    WR_INS
        RETURN

;**************************************************************
; CLEAR LCD
; W WILL BE MODIFIED!
;*************************************************************
CLEAR_LCD
        MOVLW   B'00000001'
        CALL    WR_INS
        RETURN




;*************************************************************
; POLL A KEY
; STORE THE HEX OF KEY PRESSED IN W, AND EXIT SUBROUNTINE WHEN KEY RELEASED
; INPUT: -
; OUTPUT: W
;*************************************************************
POLL_KEY
KEY_PR  btfss   KP_DA         ;Wait until data is available from the keypad
        BRA     KEY_PR
        MOVFF   KP_PORT,WREG         ;Read PortB<7:4> into W<3:0>
        andlw   0x0F
KEY_RE  btfsc   KP_DA         ;Wait until key is released
        BRA     KEY_RE
        RETURN

;*************************************************************
; POLL A KEY WHILE UPDATING RTC
; STORE THE HEX OF KEY PRESSED IN W, AND EXIT SUBROUNTINE WHEN KEY RELEASED
; INPUT: -
; OUTPUT: W
;*************************************************************
POLL_KEY_WITH_RTC
KEY_PR_RTC
        SHOW_RTC_FIRST_SCREEN
        btfss   KP_DA         ;Wait until data is available from the keypad
        BRA     KEY_PR_RTC
        MOVFF   KP_PORT,WREG         ;Read PortB<7:4> into W<3:0>
        andlw   0x0F
KEY_RE_RTC
        btfsc   KP_DA         ;Wait until key is released
        BRA     KEY_RE_RTC
        RETURN

;**************************************************************
; CONVERT A KEY PAD HEX CODE TO DECIMAL NUMBER
; INPUT: W
; OUTPUT: W
;**************************************************************
KPHexToNum
        MOVWF   TEMP_CHAR
        MOVLW   upper DIGIT         ; LOAD THE TABLE POINTER
        MOVWF   TBLPTRU             ; WITH FULL ADDRESS OF TABLE
        MOVLW   high DIGIT
        MOVWF   TBLPTRH
        MOVLW   low DIGIT
        ADDWF   TEMP_CHAR, W
        BTFSC   STATUS, 0           ; CHECK CARRY
        INCF    TBLPTRH
        MOVWF   TBLPTRL
        TBLRD*                      ;READ THE FIRST TABLE ENTRY INTO TABLAT
        MOVF    TABLAT, W
        RETURN

;**************************************************************
; CONVERT A BINARY TO LCD CHAR
; INPUT: W
; OUTPUT: W
;**************************************************************
BIN_TO_CHAR
        MOVWF   TEMP_CHAR
        MOVLW   upper NUMBER         ; LOAD THE TABLE POINTER
        MOVWF   TBLPTRU             ; WITH FULL ADDRESS OF TABLE
        MOVLW   high NUMBER
        MOVWF   TBLPTRH
        MOVLW   low NUMBER
        ADDWF   TEMP_CHAR, W
        BTFSC   STATUS, 0           ; CHECK CARRY
        INCF    TBLPTRH
        MOVWF   TBLPTRL
        TBLRD*                      ;READ THE FIRST TABLE ENTRY INTO TABLAT
        MOVF    TABLAT, W
        RETURN

;**************************************************************
; CONVERT A KEY PAD HEX CODE TO ASCII
; INPUT: W
; OUTPUT: W
;**************************************************************
KPHexToChar
        MOVWF   TEMP_CHAR
        MOVLW   upper KEY_PAD         ; LOAD THE TABLE POINTER
        MOVWF   TBLPTRU             ; WITH FULL ADDRESS OF TABLE
        MOVLW   high KEY_PAD
        MOVWF   TBLPTRH
        MOVLW   low KEY_PAD
        ADDWF   TEMP_CHAR, W
        BTFSC   STATUS, 0           ; CHECK CARRY
        INCF    TBLPTRH
        MOVWF   TBLPTRL
        TBLRD*                      ;READ THE FIRST TABLE ENTRY INTO TABLAT
        MOVF    TABLAT, W
        RETURN

Delay44us
			;439 cycles
	movlw	0x92
	movwf	DELAY1
Delay_0
	decfsz	DELAY1, f
	goto	Delay_0
                        ;1 cycles
	NOP
	return

;*************************************************************
; Delay = 0.005 seconds
; Clock frequency = 40 MHz

; Actual delay = 0.005 seconds = 50000 cycles
; Error = 0 %
;*************************************************************
Delay5ms
			;49993 cycles
	movlw	0x0E
	movwf	DELAY1
	movlw	0x28
	movwf	DELAY2
Delay_1
	decfsz	DELAY1, f
	goto	D2
	decfsz	DELAY2, f
D2
	goto	Delay_1

			;3 cycles
	goto	D3
D3
	nop

			;4 cycles (including call)
	return
end


