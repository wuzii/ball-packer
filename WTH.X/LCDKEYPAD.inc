EXTERN INIT_LCD, WR_INS, WR_DATA, SWITCH_LINE, CLEAR_LCD, POLL_KEY, POLL_KEY_WITH_RTC, KPHexToChar, KPHexToNum, BIN_TO_CHAR


;**************************************************************
; SET CURSOR AT A POSITION SPECIFIED BY THE LITERAL
; W WILL BE CHANGED!!
;************************************************************
SET_CURSOR_AT MACRO LITERAL
        MOVLW           B'10000000'
        ADDLW           LITERAL
        CALL            WR_INS
ENDM

;**************************************************************
; SET CURSOR AT A POSITION SPECIFIED BY WREG
; W WILL BE CHANGED!!
;************************************************************
SET_CURSOR_AT_W MACRO
        ADDLW           B'10000000'
        CALL            WR_INS
ENDM

;**************************************************************
; SHOW CURSOR ON LED AT CURRENT POSITION
; W WILL BE CHANGED!!
;************************************************************
SHOW_CURSOR MACRO
        MOVLW           B'00001111'
        CALL            WR_INS
ENDM


;*****************************************************************
; A MACRO THAT POLL A SPECIFIC KEY UNTIL IT'S PRESSED
; [LITERAL]: THE HEX OF THE KEY TO BE POLLED
; W WILL BE MODIFIED!!
;****************************************************************
POLL_A_KEY MACRO LITERAL
        LOCAL POLL_START
        MOVLW           LITERAL
        MOVWF           TEMP_VAR
POLL_START
        CALL            POLL_KEY
        CPFSEQ          TEMP_VAR
        BRA             POLL_START
ENDM

;*****************************************************************
; A MACRO THAT POLL A SPECIFIC KEY UNTIL IT'S PRESSED WHILE UPDATING RTC
; [LITERAL]: THE HEX OF THE KEY TO BE POLLED
; W WILL BE MODIFIED!!
;****************************************************************
POLL_A_KEY_WITH_RTC MACRO LITERAL
        LOCAL POLL_START
        MOVLW           LITERAL
        MOVWF           TEMP_VAR
POLL_START
        CALL            POLL_KEY_WITH_RTC
        CPFSEQ          TEMP_VAR
        BRA             POLL_START
ENDM

;*******************************************************************
; A MACRO THAT DISPLAY A TABLE TO LCD
; [TABLENAMETAG]:NAME TAG OF THE TABLE
; W WILL BE MODIFIED!
;******************************************************************
DISPLAY_TABLE MACRO TABLENAMETAG
        LOCAL AGAIN
        MOVLW   upper TABLENAMETAG         ; LOAD THE TABLE POINTER
        MOVWF   TBLPTRU             ; WITH FULL ADDRESS OF TABLE
        MOVLW   high TABLENAMETAG
        MOVWF   TBLPTRH
        MOVLW   low TABLENAMETAG
        MOVWF   TBLPTRL
        TBLRD*                      ;READ THE FIRST TABLE ENTRY INTO TABLAT
        MOVF    TABLAT, W
AGAIN
        CALL    WR_DATA
        TBLRD+*
        MOVF    TABLAT,W
        BNZ     AGAIN
ENDM

;*******************************************************************
; A MACRO THAT DISPLAY A TWO-DIGIT NUMBER TO LCD
; [NUM_REG]: THE REGISTER CONTAINING THE NUMBER
; [TEMP_REG1]: A BUFFER REGISTER
; [TEMP_REG2]: ANOTHER BUFFER REGISTER
; W WILL BE MODIFIED!
;******************************************************************
DISPLAY_NUMBER MACRO NUM_REG, TEMP_REG1, TEMP_REG2
        DIVIDE  NUM_REG, B'1010', TEMP_REG1, TEMP_REG2   ; DIVIDE NUMBER BY 10
        MOVF    TEMP_REG1, W        ; DISPLAY QUOTIENT, THE TENTH DIGIT
        CALL    BIN_TO_CHAR
        CALL    WR_DATA
        MOVF    TEMP_REG2, W        ; DISPLAY REMAINDER, THE UNIT DIGIT
        CALL    BIN_TO_CHAR
        CALL    WR_DATA
ENDM

;*******************************************************************
; A MACRO THAT DISPLAY A 1-DIGIT NUMBER TO LCD
; [NUM_REG]: THE REGISTER CONTAINING THE NUMBER
; W WILL BE MODIFIED!
;******************************************************************
DISPLAY_1_DIG_NUMBER MACRO NUM_REG
        MOVF    NUM_REG, W
        CALL    BIN_TO_CHAR
        CALL    WR_DATA
ENDM

;*******************************************************************
; A MACRO THAT DISPLAY A BINARY STRING
; [BIN_REG]: THE REGISTER CONTAINING THE BINARY
; [TEMP_REG]: A BUFFER REGISTER
; W WILL BE MODIFIED!
;******************************************************************
DISPLAY_BINARY MACRO BIN_REG, TEMP_REG, TEMP_REG2
        LOCAL   DISPLAY_BIN_LOOP, BIN_DISPLAY_ZERO, BIN_DISPLAY_ONE, DISPLAY_BIN_LOOP_BACK, DONE
        MOVF            BIN_REG, W
        MOVWF           TEMP_REG2
        MOVLW           B'10000000'
        MOVWF           TEMP_REG        ;THE MASK
    DISPLAY_BIN_LOOP
        MOVF            TEMP_REG, W
        ANDWF           TEMP_REG2, W
        BZ              BIN_DISPLAY_ZERO
        BRA             BIN_DISPLAY_ONE
    BIN_DISPLAY_ZERO
        MOVLW           B'00000000'
        CALL            BIN_TO_CHAR
        CALL            WR_DATA
        BRA             DISPLAY_BIN_LOOP_BACK
    BIN_DISPLAY_ONE
        MOVLW           B'00000001'
        CALL            BIN_TO_CHAR
        CALL            WR_DATA
    DISPLAY_BIN_LOOP_BACK
        BTFSC           TEMP_REG, 0
        BRA             DONE
        RRNCF           TEMP_REG, F
        BRA             DISPLAY_BIN_LOOP
DONE
ENDM

;*******************************************************************
; A MACRO THAT DISPLAY THE PATTERN
; [BIN_REG]: THE REGISTER CONTAINING THE PATTERN BINARY
; [TEMP_REG]: A BUFFER REGISTER
; W WILL BE MODIFIED!
;******************************************************************
DISPLAY_PATTERN MACRO BIN_REG, TEMP_REG, TEMP_REG2
        LOCAL   DISPLAY_BIN_LOOP, BIN_DISPLAY_ZERO, BIN_DISPLAY_ONE, DISPLAY_BIN_LOOP_BACK, DONE
        MOVF            BIN_REG, W
        MOVWF           TEMP_REG2
        MOVLW           B'00100000'
        MOVWF           TEMP_REG        ;THE MASK
    DISPLAY_BIN_LOOP
        MOVF            TEMP_REG, W
        ANDWF           TEMP_REG2, W
        BZ              BIN_DISPLAY_ZERO
        BRA             BIN_DISPLAY_ONE
    BIN_DISPLAY_ZERO
        MOVLW           B'00000000'
        CALL            BIN_TO_CHAR
        CALL            WR_DATA
        BRA             DISPLAY_BIN_LOOP_BACK
    BIN_DISPLAY_ONE
        MOVLW           B'00000001'
        CALL            BIN_TO_CHAR
        CALL            WR_DATA
    DISPLAY_BIN_LOOP_BACK
        BTFSC           TEMP_REG, 0
        BRA             DONE
        RRNCF           TEMP_REG, F
        BRA             DISPLAY_BIN_LOOP
DONE
ENDM

;*************************************************************
; THE MACRO FOR POLLING KEYS DURING TEMINATING INFO DISPLAY
; [KEY*_LINE]: THE LINE TO JUMP TO WHEN KEY* IS PRESSED
; W WILL BE MODIFIED!
; TEMP_VAR2 WILL BE MODIFIED!
;*************************************************************
ENDING_POLL_KEY  MACRO  KEYA_LINE, KEYB_LINE
    LOCAL   POLL_KEY_START, ENDING_CHECK_KEYA, ENDING_CHECK_KEYB, ENDING_CHECK_KEYD
    POLL_KEY_START
        CALL            POLL_KEY
        MOVWF           TEMP_VAR2        ;STORE KEY HEX IN TEMP_VAR2
    ENDING_CHECK_KEYA
        MOVLW           KEYA
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEYA
        BRA             ENDING_CHECK_KEYB        ; NO
        GOTO            KEYA_LINE        ; YES
    ENDING_CHECK_KEYB
        MOVLW           KEYB
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEYB
        BRA             ENDING_CHECK_KEYD        ; NO
        GOTO            KEYB_LINE        ; YES
    ENDING_CHECK_KEYD
        MOVLW           KEYD
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEYD
        BRA             POLL_KEY_START             ; NO
        GOTO            END_DISPLAY_ENDING_STATUS        ; YES
ENDM