EXTERN  REFRESH_EEPROM, EEPROM_STORE_PATTERN

READ_EEPROM MACRO   DATA_EE_ADDRH, DATA_EE_ADDR
        MOVF        DATA_EE_ADDRH, W       ;
        MOVWF       EEADRH              ; Upper bits of Data Memory Address to read
        MOVF        DATA_EE_ADDR, W        ;
        MOVWF       EEADR               ; Lower bits of Data Memory Address to read
        BCF         EECON1, EEPGD       ; Point to DATA memory
        BCF         EECON1, CFGS        ; Access EEPROM
        BSF         EECON1, RD          ; EEPROM Read
        MOVF        EEDATA, W           ; W = EEDATA
ENDM

WRITE_EEPROM MACRO  DATA_EE_ADDRH, DATA_EE_ADDR, DATA_EE_DATA
        MOVF        DATA_EE_ADDRH, W       ;
        MOVWF       EEADRH              ; Upper bits of Data Memory Address to write
        MOVF        DATA_EE_ADDR, W        ;
        MOVWF       EEADR               ; Lower bits of Data Memory Address to write
        MOVF        DATA_EE_DATA, W        ;
        MOVWF       EEDATA              ; Data Memory Value to write
        BCF         EECON1, EEPGD        ; Point to DATA memory
        BCF         EECON1, CFGS        ; Access EEPROM
        BSF         EECON1, WREN        ; Enable writes
        BCF         INTCON, GIE         ; Disable Interrupts
        MOVLW       55h                 ;
        MOVWF       EECON2              ; Write 55h
        MOVLW       0AAh                ;
        MOVWF       EECON2              ; Write 0AAh
        BSF         EECON1, WR          ; Set WR bit to begin write
        BTFSC       EECON1, WR          ; Wait for write to complete
        BRA         $-2
        BSF         INTCON, GIE         ; Enable Interrupts
ENDM