;*****************************************************************************
; SET_PATTERNS
; Prompt the user to set patterns
; PIC18F4620
; Clock Speed: 10 MHz
;****************************************************************************
ERRORLEVEL -207,-205
#include <p18f4620.inc>
    list P=18F4620, F=INHX32, C=160, N=80, ST=OFF, MM=OFF, R=DEC
#INCLUDE <LCDKEYPAD.inc>
#INCLUDE <MATH.inc>

;*******************CONSTANT DEFINES***************************************
#DEFINE     KEY1    B'00000000'
#DEFINE     KEY2    B'00000001'
#DEFINE     KEY3    B'00000010'
#DEFINE     KEYA    B'00000011'
#DEFINE     KEY4    B'00000100'
#DEFINE     KEY5    B'00000101'
#DEFINE     KEY6    B'00000110'
#DEFINE     KEYB    B'00000111'
#DEFINE     KEY7    B'00001000'
#DEFINE     KEY8    B'00001001'
#DEFINE     KEY9    B'00001010'
#DEFINE     KEYC    B'00001011'
#DEFINE     KEYS    B'00001100'
#DEFINE     KEY0    B'00001101'
#DEFINE     KEYP    B'00001110'
#DEFINE     KEYD    B'00001111'

TEMP_LCD    EQU     0x20        ;BUFFER FOR LCD INSTRUCTION
DAT         EQU     0X21        ;BUFFER FOR LCD DATA
DELAY1      EQU     0X25
DELAY2      EQU     0X26
TEMP_CHAR   EQU     0X27        ;BUFFER FOR KEY PAD CHAR

TEMP_VAR    EQU     0X28        ;BUFFER
TEMP_VAR2   EQU     0X29        ;BUFFER
NUM_BOX0    EQU     0X2A        ;STORE UNIT DIGIT OF NUMBER OF BOXES, BIT0:3 FOR NUMBER, BIT4 FOR SET/NOT SET(1/0)
NUM_BOX1    EQU     0X2B        ;STORE TENTH DIGIT OF NUMBER OF BOXES
NUM_BOX     EQU     0X2C        ;STORE NUMBER OF BOXES(NUM_BOX1*10+NUM_BOX)
TEMP_VAR3   EQU     0X2D        ;BUFFER
TEMP_VAR4   EQU     0X2E        ;BUFFER

PATTERN_STACK   EQU     0X100    ;THE PATTERN STACK STARTS AT 0X00 OF "BANK1"

code
	global SET_PATTERNS

MSG5
        DB              "Box number=", 0
MSG6
        DB              "Pattern  = (1-6)",0
MSG7
        DB              "Press D to start",0
PATTERN
        DB              B'00000000', B'00000000', B'00111111', B'00000111', B'00010010', B'00101101', B'00010101'


;*************************************************************
; THE CODE FOR SETTING THE PATTERN FOR EACH BOX
;*************************************************************
SET_PATTERNS
        CALL            CLEAR_LCD
        DISPLAY_TABLE   MSG5
        DISPLAY_NUMBER  NUM_BOX, TEMP_VAR, TEMP_VAR2    ; DISPLAY NUMBER OF BOXES
        CALL            SWITCH_LINE
        MOVLW           1               ; INITIALIZE TO 1
        MOVWF           TEMP_VAR        ; THE BOX COUNTER FOR ENTER PATTERNS
INPUT_PATTERN_LOOP
        DISPLAY_TABLE   MSG6
        SET_CURSOR_AT   H'47'
        DISPLAY_NUMBER  TEMP_VAR, TEMP_VAR2, TEMP_VAR3
        CLRF            TEMP_VAR3
INPUT_PATTERN
        SET_CURSOR_AT   H'4A'
        CALL            POLL_KEY
        MOVWF           TEMP_VAR2        ;STORE KEY HEX IN TEMP_VAR2
PATTERN_CHECK_KEY1
        MOVLW           KEY1
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY1
        BRA             PATTERN_CHECK_KEY2 ; NO
        MOVLW           1                   ; YES
        MOVWF           TEMP_VAR3
        BRA             DISPLAY_A_PATTERN
PATTERN_CHECK_KEY2
        MOVLW           KEY2
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY2
        BRA             PATTERN_CHECK_KEY3 ; NO
        MOVLW           2                   ; YES
        MOVWF           TEMP_VAR3
        BRA             DISPLAY_A_PATTERN
PATTERN_CHECK_KEY3
        MOVLW           KEY3
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY3
        BRA             PATTERN_CHECK_KEY4 ; NO
        MOVLW           3                   ; YES
        MOVWF           TEMP_VAR3
        BRA             DISPLAY_A_PATTERN
PATTERN_CHECK_KEY4
        MOVLW           KEY4
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY4
        BRA             PATTERN_CHECK_KEY5 ; NO
        MOVLW           4                   ; YES
        MOVWF           TEMP_VAR3
        BRA             DISPLAY_A_PATTERN
PATTERN_CHECK_KEY5
        MOVLW           KEY5
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY5
        BRA             PATTERN_CHECK_KEY6 ; NO
        MOVLW           5                   ; YES
        MOVWF           TEMP_VAR3
        BRA             DISPLAY_A_PATTERN
PATTERN_CHECK_KEY6
        MOVLW           KEY6
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY6
        BRA             PATTERN_CHECK_ENTER ; NO
        MOVLW           6                   ; YES
        MOVWF           TEMP_VAR3
        BRA             DISPLAY_A_PATTERN
DISPLAY_A_PATTERN
        MOVF            TEMP_VAR3, W
        CALL            BIN_TO_CHAR
        CALL            WR_DATA
        BRA             INPUT_PATTERN
PATTERN_CHECK_ENTER
        MOVLW           KEYD
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEYD
        BRA             INPUT_PATTERN       ; NO
        BRA             PATTERN_IS_ENTER_KEY     ; YES
PATTERN_IS_ENTER_KEY
        MOVF            TEMP_VAR3             ; CHECK IF A PATTERN HAS BEEN INPUTED
        BNZ             STORE_A_PATTERN     ; YES
        BRA             INPUT_PATTERN       ; NO
STORE_A_PATTERN
        LFSR            FSR0, PATTERN_STACK
        MOVF            TEMP_VAR, W
        ADDWF           FSR0L, F

        MOVLW           upper PATTERN         ; LOAD THE TABLE POINTER
        MOVWF           TBLPTRU             ; WITH FULL ADDRESS OF TABLE
        MOVLW           high PATTERN
        MOVWF           TBLPTRH
        MOVLW           low PATTERN
        ADDWF           TEMP_VAR3, W        ; ADD CURRENT PATTERN NUMBER TO THE TABLE ADDRESS
        BTFSC           STATUS, 0           ; CHECK CARRY
        INCF            TBLPTRH
        MOVWF           TBLPTRL
        TBLRD*                              ;READ THE TABLE ENTRY INTO TABLAT
        MOVF            TABLAT, W

        MOVWF           INDF0               ; STORE THE PATTERN INTO PATTERN STACK

        MOVF            TEMP_VAR, W
        CPFSEQ          NUM_BOX             ; LAST PATTERN?
        BRA             INPUT_PATTERN_LOOP_BACK
        RETURN
INPUT_PATTERN_LOOP_BACK
        INCF            TEMP_VAR, F
        SET_CURSOR_AT   H'40'
        GOTO            INPUT_PATTERN_LOOP
END

