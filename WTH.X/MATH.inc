
;**************************************************************
; TAKE IN TWO DIGITS AND STORE THEM AS A ONE-BYTE NUMBER
; W WILL BE CHANGED!!
;************************************************************
COMBINE_DIGITS MACRO UNIT_DIGIT_REG, TENTH_DIGIT_REG
        MOVF    TENTH_DIGIT_REG, W
        ANDLW   B'00001111'         ; TAKE ONLY THE LEAST 4 BITS FOR DIGIT0-9
        MULLW   10                  ; TIMES 10
        MOVF    UNIT_DIGIT_REG, W
        ANDLW   B'00001111'
        ADDWF   PRODL, W
ENDM

;**************************************************************
; THE DIVIDE FUNCTION
; INPUT: NUM_REG, THE REGISTER CONTAINING THE NUMERATOR
; INPUT: DIVISOR_LIT, LITERAL FOR DIVISOR
; OUTPUT: QUO_REG, REGISTER STORING THE QUOTIENT
; OUTPUT: REM_REG, REGISTER STORING THE REMAINDER
; W WILL BE CHANGED!!
;************************************************************
DIVIDE MACRO NUM_REG, DIVISOR_LIT, QUO_REG, REM_REG
        LOCAL   LOOP, DONE
        MOVLW   B'00000000'      ; CLEAR QUOTIENT
        MOVWF   QUO_REG
    LOOP                        ; LOOP UNTIL QUO*DIV > NUMERATOR
        MOVF    QUO_REG, W
        MULLW   DIVISOR_LIT     ; QUO * DIV
        MOVF    PRODL, W
        CPFSGT  NUM_REG
        BRA     DONE            ; NO
        INCF    QUO_REG, F
        BRA     LOOP
    DONE
        CPFSEQ  NUM_REG
        DECF    QUO_REG, F         ; DECREMENT QUO BY 1 TO GET THE ACTUAL QUOTIENT
        MOVF    QUO_REG, W
        MULLW   DIVISOR_LIT
        MOVF    PRODL, W
        SUBWF   NUM_REG, W
        MOVWF   REM_REG
ENDM