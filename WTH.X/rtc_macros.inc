	;External labels
	extern	write_rtc,read_rtc,rtc_convert,i2c_common_setup

;RTC Macros;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

rtc_resetAll	macro
;input:		none
;output:	none
;desc:		Resets all the time keeping registers on the RTC to zero
	clrf		0x74
	clrf		0x73
    call        write_rtc		;Write 0 to Seconds
    incf        0x73,f   			;Set register address to 1
	call		write_rtc
    incf        0x73,f   			;Set register address to 2
	call		write_rtc
    incf        0x73,f   			;Set register address to 3
	call		write_rtc
    incf        0x73,f   			;Set register address to 4
	call		write_rtc
    incf        0x73,f   			;Set register address to 5
	call		write_rtc
    incf        0x73,f   			;Set register address to 6
	call		write_rtc
	endm

rtc_set		macro	addliteral,datliteral
;input:		addliteral: value of address
;			datliteral: value of data
;output:	none
;desc:		loads the data in datliteral into the
;			address specified by addliteral in the RTC
	movlw	addliteral
	movwf	0x73
	movlw	datliteral
	movwf	0x74
	call	write_rtc
	endm

rtc_set_MEM		macro	addliteral
;input:		addliteral: value of address
;			0X74: value of data
;output:	none
;desc:		loads the data in 0X74 into the
;			address specified by addliteral in the RTC
	movlw	addliteral
	movwf	0x73
	call	write_rtc
	endm

rtc_read	macro	addliteral
;input:		addliteral
;output:	0x75, 0x77, 0x78
;desc:		From the selected register in the RTC, read the data
;			and load it into 0x75. 0x75 is also converted into
;			ASCII characters and the tens digit is placed into
;			0x77 and the ones digit is placed in 0x78
	movlw	addliteral
	movwf	0x73
	call	read_rtc
	movf	0x75,w
	call	rtc_convert
	endm

SHOW_RTC_FIRST_SCREEN   MACRO
        MOVLW   H'00'
        ADDLW           B'10000000'
        CALL            WR_INS
        ;SET_CURSOR_AT_W
        ;SET_CURSOR_AT   00H
        movlw	"2"
		call	WR_DATA
        movlw	"0"
		call	WR_DATA
        rtc_read	06H		;Read Address 0x06 from DS1307---year
        ;DISPLAY_BINARY  0X75, TEMP_VAR2, TEMP_VAR3
		MOVF	0x77, W
		call	WR_DATA
		MOVF	0x78, W
		call	WR_DATA

		movlw	"/"
		call	WR_DATA

		;Get month
		rtc_read	0x05		;Read Address 0x05 from DS1307---month
		MOVF	0x77, W
		call	WR_DATA
		MOVF	0x78, W
		call	WR_DATA

		movlw	"/"
		call	WR_DATA

		;Get day
		rtc_read	0x04		;Read Address 0x04 from DS1307---day
		MOVF	0x77, W
		call	WR_DATA
		MOVF	0x78, W
		call	WR_DATA

        CALL            SWITCH_LINE
        ;Get hour
		rtc_read	0x02		;Read Address 0x02 from DS1307---hour
		MOVF	0x77, W
		call	WR_DATA
		MOVF	0x78, W
		call	WR_DATA
		movlw	":"
		call	WR_DATA

		;Get minute
		rtc_read	0x01		;Read Address 0x01 from DS1307---min
		MOVF	0x77, W
		call	WR_DATA
		MOVF	0x78, W
		call	WR_DATA
		movlw	":"
		call	WR_DATA

		;Get seconds
		rtc_read	0x00		;Read Address 0x00 from DS1307---seconds
		MOVF	0x77, W
		call	WR_DATA
		MOVF	0x78, W
		call	WR_DATA
        movlw	" "
		call	WR_DATA
ENDM

SHOW_RTC    MACRO
		rtc_read	0X06		;Read Address 0x06 from DS1307---year
        ;DISPLAY_BINARY  0X75, TEMP_VAR2, TEMP_VAR3
		MOVF	0x77, W
		call	WR_DATA
		MOVF	0x78, W
		call	WR_DATA

		movlw	"/"
		call	WR_DATA

		;Get month
		rtc_read	0x05		;Read Address 0x05 from DS1307---month
		MOVF	0x77, W
		call	WR_DATA
		MOVF	0x78, W
		call	WR_DATA

		movlw	"/"
		call	WR_DATA

		;Get day
		rtc_read	0x04		;Read Address 0x04 from DS1307---day
		MOVF	0x77, W
		call	WR_DATA
		MOVF	0x78, W
		call	WR_DATA


		;Get hour
		rtc_read	0x02		;Read Address 0x02 from DS1307---hour
		MOVF	0x77, W
		call	WR_DATA
		MOVF	0x78, W
		call	WR_DATA
		movlw	":"
		call	WR_DATA

		;Get minute
		rtc_read	0x01		;Read Address 0x01 from DS1307---min
		MOVF	0x77, W
		call	WR_DATA
		MOVF	0x78, W
		call	WR_DATA
		movlw	":"
		call	WR_DATA

		;Get seconds
		rtc_read	0x00		;Read Address 0x00 from DS1307---seconds
		MOVF	0x77, W
		call	WR_DATA
		MOVF	0x78, W
		call	WR_DATA
ENDM

RTC_STORE_CURRENT_TIME MACRO
        rtc_read	0x00		;Read Address 0x00 from DS1307---SECONDS
		MOVFF	0x75, 0X74
        rtc_set_MEM 0X08        ;SENCOND -> 0X08

        rtc_read	0x01		;Read Address 0x01 from DS1307---MIN
		MOVFF	0x75, 0X74
        rtc_set_MEM 0X09        ;MIN -> 0X09

        rtc_read	0x02		;Read Address 0x02 from DS1307---HOUR
		MOVFF	0x75, 0X74
        rtc_set_MEM 0X0A        ;HOUR -> 0X0A

        rtc_read	0x04		;Read Address 0x04 from DS1307---DATE
		MOVFF	0x75, 0X74
        rtc_set_MEM 0X0B        ;DATE -> 0X0B

        rtc_read	0x05		;Read Address 0x05 from DS1307---MONTH
		MOVFF	0x75, 0X74
        rtc_set_MEM 0X0C        ;MONTH -> 0X0C

        rtc_read	0x06		;Read Address 0x05 from DS1307---YEAR
		MOVFF	0x75, 0X74
        rtc_set_MEM 0X0D        ;YEAR -> 0X0D
ENDM


;*****************************************************************
; CALCULATE THE DIFFERENCE BETWEEN CURRENT TIME AND THE STORED TIME
; [SECOND],[MIN],[SEC_OLD],[MIN_OLD]: TEMP REGS
; [DIFFERENCE]: THE OUTPUT REGISTER, CONTAIN THE TIME IN SECONDS
;*****************************************************************
RTC_TIME_DIFFERENCE MACRO   SECOND, MIN, SEC_OLD, MIN_OLD, DIFFERENCE
    LOCAL   SEC_NO_CARRY, SEC_CARRY, SEC_NO_CARRY, CHECK_MIN, MIN_NO_CARRY, MIN_CARRY, END_MACRO
        CLRF        DIFFERENCE

        rtc_read	0x00		;Read Address 0x00 from DS1307---SECONDS
		MOVFF	0x75, SECOND ;SECOND
        
        MOVLW   B'00001111'
        ANDWF   SECOND, F
        SWAPF   0X75, W
        
        ANDLW   B'00000111'
        MULLW   10
        MOVF    PRODL, W
        ADDWF   SECOND, F
        

        rtc_read	0x01		;Read Address 0x01 from DS1307---MINS
		MOVFF	0x75, MIN ;MINS
        MOVLW   B'00001111'
        ANDWF   MIN, F
        SWAPF   0X75, W
        ANDLW   B'00000111'
        MULLW   10
        MOVF    PRODL, W
        ADDWF   MIN, F

        rtc_read	0x08		;Read Address 0x00 from DS1307---SECONDS
		MOVFF	0x75, SEC_OLD ;SECOND_OLD
        MOVLW   B'00001111'
        ANDWF   SEC_OLD, F
        SWAPF   0X75, W
        ANDLW   B'00000111'
        MULLW   10
        MOVF    PRODL, W
        ADDWF   SEC_OLD, F

        rtc_read	0x09		;Read Address 0x01 from DS1307---MINS
		MOVFF	0x75, MIN_OLD ;MINS_OLD
        MOVLW   B'00001111'
        ANDWF   MIN_OLD, F
        SWAPF   0X75, W
        ANDLW   B'00000111'
        MULLW   10
        MOVF    PRODL, W
        ADDWF   MIN_OLD, F

        MOVF    SECOND, W
        CPFSGT  SEC_OLD       ;  SECOND_OLD > SECOND ?
        BRA     SEC_NO_CARRY    ; NO
        BRA     SEC_CARRY       ; YES
    SEC_NO_CARRY
        MOVF    SEC_OLD,W       
        SUBWF   SECOND,W        ; W= SEC - SEC_OLD
        ADDWF   DIFFERENCE, F   ;DIFFERENCE=DIFFERENCE+W
        BRA     CHECK_MIN
    SEC_CARRY
        MOVLW   D'60'
        ADDWF   SECOND, F       ;SECOND=SECOND+60
        MOVF    SEC_OLD,W
        SUBWF   SECOND,W        ; W= SECOND - SEC_OLD
        ADDWF   DIFFERENCE, F   ;DIFFERENCE=DIFFERENCE+W
        INCF    MIN_OLD, F      ; MIN_OLD++
    CHECK_MIN
        MOVF    MIN, W
        CPFSGT  MIN_OLD       ;  MIN_OLD > MIN ?
        BRA     MIN_NO_CARRY
        BRA     MIN_CARRY       ; YES
    MIN_NO_CARRY
        MOVF    MIN_OLD,W       ; NO
        SUBWF   MIN,W           ; W= MIN - MIN_OLD
        MULLW   60
        MOVF    PRODL, W
        ADDWF   DIFFERENCE, F   ;DIFFERENCE=DIFFERENCE+W
        BRA     END_MACRO
    MIN_CARRY
        MOVLW   D'60'
        ADDWF   MIN, F       ;MIN=MIN+60
        MOVF    MIN_OLD,W
        SUBWF   MIN,W        ; W= MIN - MIN_OLD
        MULLW   60
        MOVF    PRODL, W        ; W = W*60
        ADDWF   DIFFERENCE, F   ;DIFFERENCE=DIFFERENCE+W
    END_MACRO
ENDM

DISPLAY_TIME_DIFFERENCE MACRO DIFFERENCE, TEMP_REG1, TEMP_REG2, TEMP_REG3, TEMP_REG4
        DIVIDE  DIFFERENCE, 60, TEMP_REG1, TEMP_REG2    ;1: QUOTIENT 2: REM
        DISPLAY_NUMBER  TEMP_REG1, TEMP_REG3, TEMP_REG4 ; DISPLAY MINUTE
        MOVLW	":"
		CALL	WR_DATA
        DISPLAY_NUMBER  TEMP_REG2, TEMP_REG3, TEMP_REG4 ; DISPLAY SECONDS
ENDM

SET_RTC_TIME MACRO
        rtc_resetAll	;reset rtc

		rtc_set	0x00,	B'10000000'

		;set time
		rtc_set	0x06,	B'00010011'		; Year
		rtc_set	0x05,	B'00000100'		; Month
		rtc_set	0x04,	B'00001000'		; Date
		rtc_set	0x03,	B'00000010'		; Day
		rtc_set	0x02,	B'00001001'		; Hours
		rtc_set	0x01,	16H             ; Minutes
		rtc_set	0x00,	B'00000000'		; Seconds
ENDM