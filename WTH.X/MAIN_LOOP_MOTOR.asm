;*****************************************************************************
; MAIN_LOOP_MOTOR
; PIC18F4620
; Clock Speed: 10 MHz
;****************************************************************************
ERRORLEVEL -207,-205
#include <p18f4620.inc>
    list P=18F4620, F=INHX32, C=160, N=80, ST=OFF, MM=OFF, R=DEC
#INCLUDE <LCDKEYPAD.inc>
#INCLUDE <MATH.inc>
;*******************CONSTANT DEFINES***************************************
#DEFINE     KEY1    B'00000000'
#DEFINE     KEY2    B'00000001'
#DEFINE     KEY3    B'00000010'
#DEFINE     KEYA    B'00000011'
#DEFINE     KEY4    B'00000100'
#DEFINE     KEY5    B'00000101'
#DEFINE     KEY6    B'00000110'
#DEFINE     KEYB    B'00000111'
#DEFINE     KEY7    B'00001000'
#DEFINE     KEY8    B'00001001'
#DEFINE     KEY9    B'00001010'
#DEFINE     KEYC    B'00001011'
#DEFINE     KEYS    B'00001100'
#DEFINE     KEY0    B'00001101'
#DEFINE     KEYP    B'00001110'
#DEFINE     KEYD    B'00001111'

#DEFINE     CENTER  B'00000000'
#DEFINE     SIDE    B'00000001'

;************ DC *****************************
#DEFINE     LATCH_DC_HIGH        LATC,1         ; LATCH DC HIGHER BIT
#DEFINE     LATCH_DC_LOW         LATC,0         ; LATCH DC LOWER BIT
#DEFINE     BELT_DC              LATC,2
;*********************************************

TEMP_LCD    EQU     0x20        ;BUFFER FOR LCD INSTRUCTION
DAT         EQU     0X21        ;BUFFER FOR LCD DATA
DELAY1      EQU     0X25
DELAY2      EQU     0X26
TEMP_CHAR   EQU     0X27        ;BUFFER FOR KEY PAD CHAR

TEMP_VAR    EQU     0X28        ;BUFFER
TEMP_VAR2   EQU     0X29        ;BUFFER
NUM_BOX0    EQU     0X2A        ;STORE UNIT DIGIT OF NUMBER OF BOXES, BIT0:3 FOR NUMBER, BIT4 FOR SET/NOT SET(1/0)
NUM_BOX1    EQU     0X2B        ;STORE TENTH DIGIT OF NUMBER OF BOXES
NUM_BOX     EQU     0X2C        ;STORE NUMBER OF BOXES(NUM_BOX1*10+NUM_BOX)
TEMP_VAR3   EQU     0X2D        ;BUFFER
TEMP_VAR4   EQU     0X2E        ;BUFFER

BOX_COUNTER     EQU     0X2F    ;COUNT THE BOX
BALL_COUNTER    EQU     0X30    ;COUNT THE BALL (VALUE FROM 1 -6)
CURRENT_COLOR   EQU     0X31    ;CURRENT COLOR OF THE BALL TO DISPENSE (0/1)
CURRENT_PATTERN EQU     0X32

W_RESERV_COUNTER    EQU 0X33    ;COUNT THE NUMBER OF WHITE BALLS IN RESERVOIR
R_RESERV_COUNTER    EQU 0X34    ;COUNT THE NUMBER OF RED BALLS IN RESERVOIR

BALL_PATTERN_MASK   EQU 0X35    ;BIT MASK FOR PATTERN

PATTERN_STACK   EQU     0X100   ;THE PATTERN STACK STARTS AT 0X00 OF "BANK1"

code
	global START_BELT_DC, STOP_BELT_DC, LOWER_LATCH_MOTOR, LIFT_LATCH_MOTOR, STOP_LATCH_MOTOR
;Only these functions are visible to other asm files.

;*************************************************************
; START CONVEYOR BELT DC MOTOR
;*************************************************************
START_BELT_DC
        BSF             BELT_DC
        RETURN

;*************************************************************
; STOP CONVEYOR BELT DC MOTOR
;*************************************************************
STOP_BELT_DC
        BCF             BELT_DC
        RETURN

;*************************************************************
; LOWER THE LATCH MOTOR
;*************************************************************
LOWER_LATCH_MOTOR
        BCF             LATCH_DC_HIGH
        BSF             LATCH_DC_LOW
        RETURN

;*************************************************************
; LIFT THE LATCH MOTOR
;*************************************************************
LIFT_LATCH_MOTOR
        BCF             LATCH_DC_LOW
        BSF             LATCH_DC_HIGH
        RETURN

;*************************************************************
; STOP THE LATCH MOTOR
;*************************************************************
STOP_LATCH_MOTOR
        BCF             LATCH_DC_LOW
        BCF             LATCH_DC_HIGH
        RETURN

END