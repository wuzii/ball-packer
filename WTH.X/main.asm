;*****************************************************************************
; MAIN
; PIC18F4620
; Clock Speed: 40 MHz
;****************************************************************************
ERRORLEVEL -207,-205
#include <p18f4620.inc>
    list P=18F4620, F=INHX32, C=160, N=80, ST=OFF, MM=OFF, R=DEC
#INCLUDE <rtc_macros.inc>
#INCLUDE <LCDKEYPAD.inc>
#INCLUDE <MATH.inc>
#INCLUDE <SET_NUM_BOX.inc>
#INCLUDE <SET_PATTERNS.inc>
#INCLUDE <MAIN_LOOP_MEMORY.inc>
#INCLUDE <MAIN_LOOP_MOTOR.inc>
#INCLUDE <MAIN_LOOP_SENSOR.inc>
#INCLUDE <ISR.inc>
#INCLUDE <SERVO_TEST.inc>
#INCLUDE <SENSOR_TEST.inc>
#INCLUDE <PERM_LOG.inc>

;********************Configuration Bits**************************************

    CONFIG OSC=HSPLL, FCMEN=OFF, IESO=OFF
    CONFIG PWRT=OFF, BOREN=SBORDIS, BORV=3
    CONFIG WDT=OFF, WDTPS=32768
    CONFIG MCLRE=ON, LPT1OSC=OFF, PBADEN=OFF, CCP2MX=PORTC
    CONFIG STVREN=ON, LVP=OFF, XINST=OFF
    CONFIG DEBUG=OFF
    CONFIG CP0=OFF, CP1=OFF, CP2=OFF, CP3=OFF
    CONFIG CPB=OFF, CPD=OFF
    CONFIG WRT0=OFF, WRT1=OFF, WRT2=OFF, WRT3=OFF
    CONFIG WRTB=OFF, WRTC=OFF, WRTD=OFF
    CONFIG EBTR0=OFF, EBTR1=OFF, EBTR2=OFF, EBTR3=OFF
    CONFIG EBTRB=OFF

;*******************CONSTANT DEFINATIONS***************************************
#DEFINE     KEY1    B'00000000'
#DEFINE     KEY2    B'00000001'
#DEFINE     KEY3    B'00000010'
#DEFINE     KEYA    B'00000011'
#DEFINE     KEY4    B'00000100'
#DEFINE     KEY5    B'00000101'
#DEFINE     KEY6    B'00000110'
#DEFINE     KEYB    B'00000111'
#DEFINE     KEY7    B'00001000'
#DEFINE     KEY8    B'00001001'
#DEFINE     KEY9    B'00001010'
#DEFINE     KEYC    B'00001011'
#DEFINE     KEYS    B'00001100'
#DEFINE     KEY0    B'00001101'
#DEFINE     KEYP    B'00001110'
#DEFINE     KEYD    B'00001111'

#DEFINE     CENTER  B'00000000'
#DEFINE     SIDE    B'00000001'

;*************** Sensors************************
#DEFINE     BOX_POS_SENSOR      PORTC,5
#DEFINE     CENTER_PIPE_SENSOR  PORTC,6
#DEFINE     BOTTOM_PIPE_SENSOR  PORTC,7
#DEFINE     LATCH_SENSOR_LOWER  PORTB,6
#DEFINE     LATCH_SENSOR_UPPER  PORTB,7
#DEFINE     EM_STOP_SWITCH      PORTB,0
;*********************************************

;************ DC *****************************
#DEFINE     LATCH_DC_HIGH        LATC,1         ; LATCH DC HIGHER BIT
#DEFINE     LATCH_DC_LOW         LATC,0         ; LATCH DC LOWER BIT
#DEFINE     BELT_DC              LATC,2
;*********************************************


;**********  Servo  OCS FREQ=40MHz, TMR0 PRESCALER=4
#DEFINE     PWM_PERIOD          B'11001000'             ;200*0.1ms=20ms
#DEFINE     TIMER0_RESET        B'00000110'     ;6, TMR0 COUNT FROM 6-255
#DEFINE     UPPER_SERVO         LATE,0
#DEFINE     LOWER_SERVO         LATE,1
#DEFINE     SERVO_CENTER_MARK   B'10111001'     ;185
#DEFINE     UPPER_POS_WHITE     B'11000001'     ;193
#DEFINE     UPPER_POS_RED       B'10110001'     ;177
#DEFINE     UPPER_POS_CENTER    B'10111011'     ;187

#DEFINE     LOWER_POS_0         B'10110100'     ;180
#DEFINE     LOWER_POS_1         B'10110110'     ;182
#DEFINE     LOWER_POS_2         B'10111000'     ;184
#DEFINE     LOWER_POS_3         B'10111010'     ;186
#DEFINE     LOWER_POS_4         B'10111101'     ;189
#DEFINE     LOWER_POS_5         B'10111111'     ;191
;***************

TEMP_LCD    EQU     0x20        ;BUFFER FOR LCD INSTRUCTION
DAT         EQU     0X21        ;BUFFER FOR LCD DATA
DELAY1      EQU     0X25
DELAY2      EQU     0X26
TEMP_CHAR   EQU     0X27        ;BUFFER FOR KEY PAD CHAR

TEMP_VAR    EQU     0X28        ;BUFFER
TEMP_VAR2   EQU     0X29        ;BUFFER
NUM_BOX0    EQU     0X2A        ;STORE UNIT DIGIT OF NUMBER OF BOXES, BIT0:3 FOR NUMBER, BIT4 FOR SET/NOT SET(1/0)
NUM_BOX1    EQU     0X2B        ;STORE TENTH DIGIT OF NUMBER OF BOXES
NUM_BOX     EQU     0X2C        ;STORE NUMBER OF BOXES(NUM_BOX1*10+NUM_BOX)
TEMP_VAR3   EQU     0X2D        ;BUFFER
TEMP_VAR4   EQU     0X2E        ;BUFFER

BOX_COUNTER     EQU     0X2F    ;COUNT THE BOX
BALL_COUNTER    EQU     0X30    ;COUNT THE BALL (VALUE FROM 1 -6)
CURRENT_COLOR   EQU     0X31    ;CURRENT COLOR OF THE BALL TO DISPENSE (0/1)
CURRENT_PATTERN EQU     0X32



W_RESERV_COUNTER    EQU 0X33    ;COUNT THE NUMBER OF WHITE BALLS IN RESERVOIR
R_RESERV_COUNTER    EQU 0X34    ;COUNT THE NUMBER OF RED BALLS IN RESERVOIR

BALL_PATTERN_MASK   EQU 0X35    ;BIT MASK FOR PATTERN

SERVO_PERIOD_COUNT  EQU 0X36    ;COUNT FROM 0 TO 200 FOR 20MS
UPPER_SERVO_MARK    EQU 0X37    ;MARK DUTY CYCLE LENGTH = (200-THIS)*0.1ms, [180-190]
LOWER_SERVO_MARK    EQU 0X38

WREG_TEMP           EQU 0X39    ;STACK REGS FOR INTERRUPT(LOW)
STATUS_TEMP         EQU 0X3A
BSR_TEMP            EQU 0X3B

WREG_TEMP_H         EQU 0X3C    ;STACK REGS FOR INTERRUPT(HIGH)
STATUS_TEMP_H       EQU 0X3D
BSR_TEMP_H          EQU 0X3E

ENDING_STATUS_PAGE   EQU 0X3F   ; NUMBER OF PAGES IN ENDING STATUS

TIME_DIFFERENCE     EQU 0X40

W_RES_INC           EQU 0X41    ; NUMBER OF BALLS TO BE INCREMENTED (0 OR 1)
R_RES_INC           EQU 0X42

W_RES_DEB_COUNTER   EQU 0X43    ; COUNTERS FOR RESERVOIR DEBOUNCING TIMER
R_RES_DEB_COUNTER   EQU 0X44

TEMP_VAR5           EQU 0X45
TEMP_VAR6           EQU 0X46

PAGE_NUM            EQU 0X47

PATTERN_STACK   EQU     0X100   ;THE PATTERN STACK STARTS AT 0X00 OF "BANK1"
;******************VECTORS*****************************************
        ORG             0X0000
        GOTO            INIT
        ORG             0X08
        CALL            ISR_GENERAL_HIGH
        RETFIE
        ORG             0X18
        CALL            ISR_GENERAL_LOW
        RETFIE


MSG1
        DB              "<IDLE>", 0    ;DECLEAR TABLE
MSG2
        DB              "START:D", 0    ;DECLEAR TABLE

MSG5
        DB              "Box number=", 0
MSG6
        DB              "Pattern  = (1-6)",0
MSG7
        DB              "Press D to start",0
PATTERN
        DB              B'00000000', B'00000000', B'00111111', B'00000111', B'00010010', B'00101101', B'00010101'
MSG8
        DB              "BOX:  /  ",0
MSG9
        DB              "Completed!  1/",0
MSG10
        DB              "For info press A",0
MSG17
        DB              "Calibrating..",0
MSG18
        DB              "START:D",0
INIT
        CLRF            NUM_BOX
        CLRF            TEMP_VAR
        CLRF            TEMP_VAR2
        CLRF            NUM_BOX0
        CLRF            NUM_BOX1
        CLRF            TEMP_VAR3
        CLRF            TEMP_VAR4
        CLRF            BOX_COUNTER
        CLRF            BALL_COUNTER
        CLRF            BOX_COUNTER
        CLRF            BALL_COUNTER
        CLRF            W_RESERV_COUNTER
        CLRF            R_RESERV_COUNTER
        CLRF            SERVO_PERIOD_COUNT
        CLRF            W_RES_INC
        CLRF            R_RES_INC
        CLRF            W_RES_DEB_COUNTER
        CLRF            R_RES_DEB_COUNTER

        MOVLW           0Fh         ; Configure A/D
        MOVWF           ADCON1      ; for digital inputs
        MOVWF           07h         ; Configure comparators
        MOVWF           CMCON       ; for digital input

;*************PORT I/O INITIALIZATION******************
        SETF            TRISA       ; INPUT
        SETF            TRISB       ; INPUT
        
        SETF            TRISC
        BCF             TRISC, 0
        BCF             TRISC, 1
        BCF             TRISC, 2
        ;MOVLW           B'11111000' ; 0,1,2 OUTPUT 3,4,5,6,7 INPUT
        CLRF            TRISD       ; OUTPUT
        CLRF            TRISE       ; OUTPUT
        CLRF            PORTA
        CLRF            PORTB
        CLRF            PORTC
        CLRF            LATC
;*************************************************

        BSF             RCON, 7     ; PRIORITY MODE ENABLED
        CLRF            PIR1        ; CLEAR INTERRUPT FLAGS
        CLRF            PIR2

        BSF             INTCON,7    ; ENABLE GLOBAL HIGH PRIORITY INTERRUPT
        BSF             INTCON,6    ; ENABLE GLOBAL LOW PRIORITY INTERRUPT

;****************TIMER0 AND SERVOS*************************************
        MOVLW           SERVO_CENTER_MARK   ; CENTER THE SERVOS BEFORE STARTING INTERRUPT
        MOVWF           UPPER_SERVO_MARK
        MOVWF           LOWER_SERVO_MARK

        MOVLW           TIMER0_RESET    ; SET TIMER0
        MOVWF           TMR0L

        BCF             INTCON2,2   ; SET TIMER0 TO LOW PRIORITY
        BSF             INTCON,5    ; ENABLE TIMER0 OVERFLOW INTERRUPT
        MOVLW           B'11000001' ; ENABLE TIMER0, 8 BIT MODE, PRESCALER=4
        MOVWF           T0CON
;***********************************************************************


;************** RESERVIOR COUNTERS INTERRUPT******************************
        BSF             INTCON2, 5  ; EX INTERRUPT1 ON RISING EDGE FOR RESERVOIR COUNTER
        BSF             INTCON2, 4  ; EX INTERRUPT2
        BSF             INTCON3, 7  ; EX INT2 HIGHT PRIORITY FOR RESERVOIR COUNTER
        BSF             INTCON3, 6  ; EX INT1
        BSF             INTCON2, 7  ; TURN OFF INTERNAL PULL UP
        BCF             INTCON3, 1  ; CLEAR FLAG BITS
        BCF             INTCON3, 0
        BSF             INTCON3, 4  ; ENABLE EX INT2
        BSF             INTCON3, 3  ; ENABLE EX INT1
;************************************************************************

;*****************LATCH INTERRUPT*****************************************
        BSF             INTCON2, 0 ; SET INT ON CHANGE TO HIGH PRIORITY
        BCF             INTCON, 0 ; CLEAR INT ON CHANGE FLAG BIT
        BSF             INTCON, 3 ; ENABLE INTERRUPT ON BIT CHANGE
;************************************************************************
        CALL       	    i2c_common_setup
		
        CALL            INIT_LCD
        ;rtc_resetAll
        ;SET_RTC_TIME


TEST
        ;CALL            REFRESH_EEPROM

        ;CALL            CLEAR_LCD
        ;SHOW_RTC
        ;CALL            TEST_SENSOR
        ;CALL            TEST_SERVO
;TEST_END
;        BRA             TEST_END
IDLE
        CALL            CLEAR_LCD
        DISPLAY_TABLE   MSG17
        CALL            SWITCH_LINE
        DISPLAY_TABLE   MSG18
        CALL            TEST_SERVO              ; CALIBRATION BEFORE START
        CALL            CLEAR_LCD
        SHOW_RTC_FIRST_SCREEN
        DISPLAY_TABLE   MSG2
        POLL_A_KEY_WITH_RTC      KEYD            ; PRESS D TO START

        CALL            SET_NUM_BOX     ; USER ENTER THE NUMBER OF BOXES
        CALL            SET_PATTERNS    ; USER ENTER PATTERN FOR EACH BOX

PROMPT_FOR_START
        CALL            CLEAR_LCD
        DISPLAY_TABLE   MSG7
        CALL            SWITCH_LINE
        POLL_A_KEY      KEYD            ; PRESS D TO START

        BCF             INTCON3, 4
        BCF             INTCON3, 3
;        MOVLW           D'15'
;        MOVWF           TEMP_VAR
;        MOVLW           D'13'
;        CPFSGT          W_RESERV_COUNTER        ; IF BALL NUM>=14?
;        BRA             HACK_CHECK_R_RESERV
;        MOVFF           TEMP_VAR, W_RESERV_COUNTER
;HACK_CHECK_R_RESERV
;        MOVLW           D'13'
;        CPFSGT          R_RESERV_COUNTER        ; IF BALL NUM>=14?
;        BRA             HACK_END
;        MOVFF           TEMP_VAR, R_RESERV_COUNTER
HACK_END
        RTC_STORE_CURRENT_TIME          ;STORE CURRENT TIME IN RTC GENERAL REGISTERS
        CLRF            BOX_COUNTER     ; INITIALIZE BOX_COUNTER
OUTER_LOOP_MOVE_BOX
        CLRF            BALL_COUNTER    ; INITIALIZE BALL_COUNTER TO 0
        MOVLW           B'01000000'
        MOVWF           BALL_PATTERN_MASK   ;INITALIZE BALL_PATTERN_MASK

        INC_BALL_COUNTER                    ; BALL_COUNTER++

        CALL            UPDATE_CURRENT_PATTERN
        CALL            UPDATE_CURRENT_COLOR

        CALL            DISPLAY_STATUS

        ROTATE_UPPER    SIDE              ; LOAD ONE BALL FROM RESERV TO UPPER DISK
        ROTATE_LOWER                      ; ROTATE LOWER DISK TO BALL_COUNTER POSITION
        CALL            DEC_RESERV_COUNTER   ; DECREMENT A RESERVIOR COUNTER
        CALL            START_BELT_DC

        ;CALL            DELAY_2S
        CALL            DELAY_250MS
        POLL_SENSOR_CLEAR BOX_POS_SENSOR    ; POLL THE BOX POSITION SENSOR UNTIL LAST BOX IS AWAY, IR SENSOR CLEAR => HIGH
        POLL_SENSOR_SET BOX_POS_SENSOR
        
        CALL            STOP_BELT_DC

        ;DECFSZ          BOX_COUNTER, W      ; CHECK IF THIS IS THE FIRST BOX
        TSTFSZ          BOX_COUNTER         ; CHECK IF THIS IS THE FIRST BOX
        CALL            DELAY_250MS
        CALL            LOWER_LATCH_MOTOR
        CALL            DELAY_250MS
        CALL            LOWER_LATCH_MOTOR
        GOTO            INNER_LOOP_DROP_BALL    ; IF FIRST BOX THEN SKIP LATCH SNAPPING
INNER_LOOP_DROP_BALL
        CALL            DISPLAY_STATUS
        CALL            DELAY_250MS
        ROTATE_UPPER    CENTER              ; ROTATE UPPER DISK TO CENTER TO DROP THE BALL
        INC_BALL_COUNTER                    ; BALL_COUNTER++

        CALL            DELAY_250MS
        CALL            DELAY_250MS
        CALL            DELAY_250MS

        ;POLL_SENSOR_CLEAR CENTER_PIPE_SENSOR  ; WAIT TILL THE BALL FULLY OUT OF UPPER DISK -> SENSOR BLOCKED

        MOVF            BALL_COUNTER, W     ; CHECK IF BALL_COUNTER == 6
        SUBLW           7                   ; ==6 ??
        BZ              OUTER_LOOP_MOVE_BOX_LOOP_BACK   ; YES

        CALL            UPDATE_CURRENT_COLOR    ; NO => LOAD THE NEXT COLOR

        ROTATE_UPPER    SIDE              ; LOAD A BALL INTO UPPER DISK
        CALL            DEC_RESERV_COUNTER      ; DECREMENT A RESERVIOR COUNTER

        ;POLL_SENSOR_CLEAR BOTTOM_PIPE_SENSOR  ; WAIT TILL THE BALL FULLY OUT OF LOWER DISK => SENSOR BLOCKED
        
        ROTATE_LOWER                     ; ROTATE LOWER DISK TO BALL_COUNTER POSITION

        CALL            DELAY_250MS
        CALL            DELAY_250MS
        CALL            DELAY_250MS

        GOTO            INNER_LOOP_DROP_BALL

OUTER_LOOP_MOVE_BOX_LOOP_BACK
        ;POLL_SENSOR_CLEAR BOTTOM_PIPE_SENSOR  ; WAIT TILL THE BALL FULLY OUT OF LOWER DISK => BOTTOM SENSOR BLOCKED
        INCF            BOX_COUNTER, F      ; BOX_COUNTER++

        MOVF            BOX_COUNTER, W     ; CHECK IF NUM_BOX==BOX_COUNTER
        CPFSEQ          NUM_BOX
        GOTO            OUTER_LOOP_MOVE_BOX ; NO, LOOP FOR NEXT BOX
        BRA             ENDING_SEQUENCE     ; YES, THEN IS LAST BOX
ENDING_SEQUENCE
        CALL            START_BELT_DC
        CALL            DELAY_2S
        CALL            DELAY_250MS
        CALL            DELAY_250MS
        CALL            DELAY_250MS
        CALL            DELAY_250MS
        CALL            DELAY_250MS
        CALL            DELAY_250MS
        CALL            DELAY_250MS
        CALL            DELAY_250MS

        ;POLL_SENSOR_SET BOX_POS_SENSOR    ; POLL THE BOX POSITION SENSOR UNTIL LAST BOX IS UNDER LATCH SNAPPER, IR SENSOR CLEAR => HIGH
        CALL            STOP_BELT_DC
        CALL            LOWER_LATCH_MOTOR   ; CLOSE LATCH FOR LAST BOX
        CALL            DELAY_250MS
        CALL            LOWER_LATCH_MOTOR
        CALL            DELAY_2S

        CALL            START_BELT_DC
        CALL            DELAY_2S            ; MOVE LAST BOX TO PICK UP AREA
        CALL            DELAY_250MS
        CALL            DELAY_250MS
        CALL            DELAY_250MS
        CALL            DELAY_250MS
        CALL            DELAY_250MS

        CALL            STOP_BELT_DC
TEMINATING_INFO
        CALL            EEPROM_STORE_PATTERN
        RTC_TIME_DIFFERENCE         TEMP_VAR, TEMP_VAR2, TEMP_VAR3, TEMP_VAR4, TIME_DIFFERENCE
        CALL            DISPLAY_ENDING_STATUS
        GOTO            INIT
STOP    BRA             STOP


; Delay = 0.25 seconds
; Clock frequency = 40 MHz

; Actual delay = 0.25 seconds = 2500000 cycles
; Error = 0 %


DELAY_250MS
			;2499992 cycles
	movlw	0x15
	movwf	TEMP_VAR
	movlw	0x74
	movwf	TEMP_VAR2
	movlw	0x06
	movwf	TEMP_VAR3
DELAY_250MS_0
	decfsz	TEMP_VAR, f
	goto	DELAY_250MS_1
	decfsz	TEMP_VAR2, f
DELAY_250MS_1
	goto	DELAY_250MS_2
	decfsz	TEMP_VAR3, f
DELAY_250MS_2
	goto	DELAY_250MS_0

			;4 cycles
	goto	DELAY_250MS_3
DELAY_250MS_3
	goto	DELAY_250MS_4
DELAY_250MS_4
			;4 cycles (including call)
	return


; Delay = 2 seconds
; Clock frequency = 40 MHz

; Actual delay = 2 seconds = 20000000 cycles
; Error = 0 %

DELAY_2S
			;19999992 cycles
	movlw	0xB5
	movwf	TEMP_VAR
	movlw	0x99
	movwf	TEMP_VAR2
	movlw	0x2C
	movwf	TEMP_VAR3
DELAY_2S_0
	decfsz	TEMP_VAR, f
	goto	DELAY_2S_1
	decfsz	TEMP_VAR2, f
DELAY_2S_1
	goto	DELAY_2S_2
	decfsz	TEMP_VAR3, f
DELAY_2S_2
	goto	DELAY_2S_0

			;4 cycles
	goto	DELAY_2S_3
DELAY_2S_3
	goto	DELAY_2S_4
DELAY_2S_4
			;4 cycles (including call)
	return

END




