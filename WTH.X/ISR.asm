;*****************************************************************************
; ISR
; PIC18F4620
; Clock Speed: 40 MHz
;****************************************************************************
ERRORLEVEL -207,-205
#include <p18f4620.inc>
    list P=18F4620, F=INHX32, C=160, N=80, ST=OFF, MM=OFF, R=DEC
#INCLUDE <MAIN_LOOP_MOTOR.inc>

;Servo**********    OCS FREQ=40MHz, TMR0 PRESCALER=4
#DEFINE     PWM_PERIOD          B'11001000'             ;200*0.1ms=20ms
#DEFINE     TIMER0_RESET        B'00000110'     ;6, TMR0 COUNT FROM 6-255
#DEFINE     UPPER_SERVO         LATE,0
#DEFINE     LOWER_SERVO         LATE,1
#DEFINE     SERVO_CENTER_MARK   B'10111001'     ;185
#DEFINE     UPPER_POS_WHITE     B'10111101'     ;189
#DEFINE     UPPER_POS_RED       B'10110101'     ;181
#DEFINE     UPPER_POS_CENTER    B'10111011'     ;187

#DEFINE     LOWER_POS_0         B'10110100'     ;180
#DEFINE     LOWER_POS_1         B'10110110'     ;182
#DEFINE     LOWER_POS_2         B'10111000'     ;184
#DEFINE     LOWER_POS_3         B'10111010'     ;186
#DEFINE     LOWER_POS_4         B'10111100'     ;188
#DEFINE     LOWER_POS_5         B'10111110'     ;190
;***************

;*************** RESERVOIR DEBOUNCING TIME************
#DEFINE     W_RES_DEB_TIME      250             ;20MS
#DEFINE     R_RES_DEB_TIME      250             ;20MS
;*****************************************************

;*************** Sensors************************
#DEFINE     BOX_POS_SENSOR      PORTC,5
#DEFINE     CENTER_PIPE_SENSOR  PORTC,6
#DEFINE     BOTTOM_PIPE_SENSOR  PORTC,7
#DEFINE     LATCH_SENSOR_LOWER  PORTB,6
#DEFINE     LATCH_SENSOR_UPPER  PORTB,7
#DEFINE     EM_STOP_SWITCH      PORTB,0
;*********************************************

;************ DC *****************************
#DEFINE     LATCH_DC_HIGH        LATC,1         ; LATCH DC HIGHER BIT
#DEFINE     LATCH_DC_LOW         LATC,0         ; LATCH DC LOWER BIT
#DEFINE     BELT_DC              LATC,2
;*********************************************

TEMP_LCD    EQU     0x20        ;BUFFER FOR LCD INSTRUCTION
DAT         EQU     0X21        ;BUFFER FOR LCD DATA
DELAY1      EQU     0X25
DELAY2      EQU     0X26
TEMP_CHAR   EQU     0X27        ;BUFFER FOR KEY PAD CHAR

TEMP_VAR    EQU     0X28        ;BUFFER
TEMP_VAR2   EQU     0X29        ;BUFFER
NUM_BOX0    EQU     0X2A        ;STORE UNIT DIGIT OF NUMBER OF BOXES, BIT0:3 FOR NUMBER, BIT4 FOR SET/NOT SET(1/0)
NUM_BOX1    EQU     0X2B        ;STORE TENTH DIGIT OF NUMBER OF BOXES
NUM_BOX     EQU     0X2C        ;STORE NUMBER OF BOXES(NUM_BOX1*10+NUM_BOX)
TEMP_VAR3   EQU     0X2D        ;BUFFER
TEMP_VAR4   EQU     0X2E        ;BUFFER

BOX_COUNTER     EQU     0X2F    ;COUNT THE BOX
BALL_COUNTER    EQU     0X30    ;COUNT THE BALL (VALUE FROM 1 -6)
CURRENT_COLOR   EQU     0X31    ;CURRENT COLOR OF THE BALL TO DISPENSE (0/1)
CURRENT_PATTERN EQU     0X32



W_RESERV_COUNTER    EQU 0X33    ;COUNT THE NUMBER OF WHITE BALLS IN RESERVOIR
R_RESERV_COUNTER    EQU 0X34    ;COUNT THE NUMBER OF RED BALLS IN RESERVOIR

BALL_PATTERN_MASK   EQU 0X35    ;BIT MASK FOR PATTERN

SERVO_PERIOD_COUNT  EQU 0X36    ;COUNT FROM 0 TO 200 FOR 20MS
UPPER_SERVO_MARK    EQU 0X37    ;MARK DUTY CYCLE LENGTH = (200-THIS)*0.1ms, [180-190]
LOWER_SERVO_MARK    EQU 0X38

WREG_TEMP           EQU 0X39    ;STACK REGS FOR INTERRUPT(LOW)
STATUS_TEMP         EQU 0X3A
BSR_TEMP            EQU 0X3B

WREG_TEMP_H         EQU 0X3C    ;STACK REGS FOR INTERRUPT(HIGH)
STATUS_TEMP_H       EQU 0X3D
BSR_TEMP_H          EQU 0X3E

W_RES_INC           EQU 0X41    ; NUMBER OF BALLS TO BE INCREMENTED
R_RES_INC           EQU 0X42

W_RES_DEB_COUNTER   EQU 0X43    ; COUNTERS FOR RESERVOIR DEBOUNCING TIMER
R_RES_DEB_COUNTER   EQU 0X44

PATTERN_STACK   EQU     0X100   ;THE PATTERN STACK STARTS AT 0X00 OF "BANK1"

code
	global ISR_GENERAL_HIGH, ISR_GENERAL_LOW


ISR_GENERAL_HIGH
        MOVWF           WREG_TEMP_H
        MOVFF           STATUS, STATUS_TEMP_H
        MOVFF           BSR, BSR_TEMP_H
        BTFSC           INTCON, 0        ;TEST INT-ON-CHANGE
        CALL            ISR_INT_ON_CHANGE
        BTFSC           INTCON3,1        ;TEST EX INT2
        BRA             INC_W_RESERVOIR
        BTFSC           INTCON3,0        ;TEST EX INT1
        BRA             INC_R_RESERVOIR
        BRA             END_ISR_HIGH
INC_W_RESERVOIR
        BTFSC           W_RES_INC, 0        ; IF W_RES_INC = 0, DON'T INCREMENT
        INCF            W_RESERV_COUNTER    ; WHITE RESERVOIR COUNTER++
        CLRF            W_RES_INC
        CLRF            W_RES_DEB_COUNTER
        BCF             INTCON3,1           ; CLEAR INTERRUPT FLAG
        BRA             END_ISR_HIGH
INC_R_RESERVOIR
        BTFSC           R_RES_INC, 0
        INCF            R_RESERV_COUNTER    ; RED RESERVOIR COUNTER++
        CLRF            R_RES_INC
        CLRF            R_RES_DEB_COUNTER
        BCF             INTCON3,0           ; CLEAR INTERRPT FLAG
END_ISR_HIGH
        MOVF            WREG_TEMP_H, W
        MOVFF           STATUS_TEMP_H, STATUS
        MOVFF           BSR_TEMP_H, BSR
        RETURN

ISR_INT_ON_CHANGE
        BTFSC           LATCH_SENSOR_LOWER        ; TEST IF LOWER LATCH SWITCH CLOSED
        BRA             LIFT_LATCH
        BTFSC           LATCH_SENSOR_UPPER  ; TEST IF UPPER LATCH SWITCH CLOSED
        BRA             STOP_LATCH
        BCF             INTCON, 0           ; CLEAR FLAG BIT
        RETURN
STOP_LATCH
        ;MOVFF           LATC, TEMP_VAR
        ;BTFSS           LATCH_DC_HIGH
        ;BTFSS           TEMP_VAR,1
        ;BRA             STOP_LATCH_RETURN
        BTFSC           LATCH_DC_HIGH
        CALL            STOP_LATCH_MOTOR    ; YES => STOP LATCH MOTOR
        BTG             PORTD, 0
        BCF             INTCON, 0           ; CLEAR FLAG BIT
STOP_LATCH_RETURN
        RETURN
LIFT_LATCH
        CALL            LIFT_LATCH_MOTOR    ; YES => LIFT LATCH MOTOR
        BTG             PORTD, 1
        BCF             INTCON, 0           ; CLEAR FLAG BIT
        RETURN


ISR_GENERAL_LOW
        MOVWF           WREG_TEMP
        MOVFF           STATUS, STATUS_TEMP
        MOVFF           BSR, BSR_TEMP
        BTFSC           INTCON,2        ;TEST FOR TIMER0
        CALL            ISR_TIMER0
        MOVF            WREG_TEMP, W
        MOVFF           STATUS_TEMP, STATUS
        MOVFF           BSR_TEMP, BSR
        RETURN

ISR_TIMER0
        BCF             INTCON,5

RES_DEBOUNCING_W
        BTFSC           W_RES_INC, 0        ; CHECK IF W_RES_INC = 1
        BRA             RES_DEBOUNCING_R    ; YES
        INCF            W_RES_DEB_COUNTER   ; NO
        MOVLW           W_RES_DEB_TIME
        CPFSGT          W_RES_DEB_COUNTER
        BRA             RES_DEBOUNCING_R
        BSF             W_RES_INC, 0        ; IF COUNTER==200, => TIME=20ms, SET W_RES_INC=1
        CLRF            W_RES_DEB_COUNTER
RES_DEBOUNCING_R
        BTFSC           R_RES_INC, 0        ; CHECK IF W_RES_INC = 1
        BRA             SERVO_TIMER_CODE    ; YES
        INCF            R_RES_DEB_COUNTER   ; NO
        MOVLW           R_RES_DEB_TIME
        CPFSGT          R_RES_DEB_COUNTER
        BRA             SERVO_TIMER_CODE
        BSF             R_RES_INC, 0        ; IF COUNTER==200, => TIME=20ms, SET W_RES_INC=1
        CLRF            R_RES_DEB_COUNTER

SERVO_TIMER_CODE
        MOVLW           PWM_PERIOD
        CPFSLT          SERVO_PERIOD_COUNT  ; IF COUNTER==200, => TIME=20ms
        BRA             ON_20MS
        MOVF            SERVO_PERIOD_COUNT, W
        CPFSEQ          UPPER_SERVO_MARK    ; REACHED THE UPPER SERVO MARK
        BRA             TEST_LOWER_SERVO
        BSF             UPPER_SERVO         ; SET HIGH TO GENERATE A PULSE
TEST_LOWER_SERVO
        CPFSEQ          LOWER_SERVO_MARK    ; REACHED THE LOWER SERVO MARK
        BRA             INC_SERVO_COUNT
        BSF             LOWER_SERVO         ; SET HIGH TO GENERATE A PULSE
INC_SERVO_COUNT
        INCF            SERVO_PERIOD_COUNT
        BRA             END_ISR_TIMER0
ON_20MS
        CLRF            SERVO_PERIOD_COUNT  ; CLEAR COUNTER
        BCF             UPPER_SERVO         ; CLEAR TO STOP PULSE
        BCF             LOWER_SERVO
END_ISR_TIMER0
        BCF             INTCON,2            ; CLEAR FLAG
        MOVLW           TIMER0_RESET        ; SET TIMER0
        MOVWF           TMR0L
        BSF             INTCON,5
        RETURN
end