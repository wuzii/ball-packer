;*****************************************************************************
; SERVO_TEST
; PIC18F4620
; Clock Speed: 40 MHz
;****************************************************************************
ERRORLEVEL -207,-205
#include <p18f4620.inc>
    list P=18F4620, F=INHX32, C=160, N=80, ST=OFF, MM=OFF, R=DEC
#INCLUDE <LCDKEYPAD.inc>
#INCLUDE <MAIN_LOOP_MOTOR.inc>

;*******************CONSTANT DEFINES***************************************
#DEFINE     KEY1    B'00000000'
#DEFINE     KEY2    B'00000001'
#DEFINE     KEY3    B'00000010'
#DEFINE     KEYA    B'00000011'
#DEFINE     KEY4    B'00000100'
#DEFINE     KEY5    B'00000101'
#DEFINE     KEY6    B'00000110'
#DEFINE     KEYB    B'00000111'
#DEFINE     KEY7    B'00001000'
#DEFINE     KEY8    B'00001001'
#DEFINE     KEY9    B'00001010'
#DEFINE     KEYC    B'00001011'
#DEFINE     KEYS    B'00001100'
#DEFINE     KEY0    B'00001101'
#DEFINE     KEYP    B'00001110'
#DEFINE     KEYD    B'00001111'

;Servo**********    OCS FREQ=40MHz, TMR0 PRESCALER=4
#DEFINE     PWM_PERIOD          B'11001000'             ;200*0.1ms=20ms
#DEFINE     TIMER0_RESET        B'00000110'     ;6, TMR0 COUNT FROM 6-255
#DEFINE     UPPER_SERVO         LATE,0
#DEFINE     LOWER_SERVO         LATE,1
#DEFINE     SERVO_CENTER_MARK   B'10111001'     ;185
#DEFINE     UPPER_POS_WHITE     B'11000001'     ;193
#DEFINE     UPPER_POS_RED       B'10110001'     ;177
#DEFINE     UPPER_POS_CENTER    B'10111011'     ;187

#DEFINE     LOWER_POS_0         B'10110100'     ;180
#DEFINE     LOWER_POS_1         B'10110110'     ;182
#DEFINE     LOWER_POS_2         B'10111000'     ;184
#DEFINE     LOWER_POS_3         B'10111010'     ;186
#DEFINE     LOWER_POS_4         B'10111101'     ;189
#DEFINE     LOWER_POS_5         B'10111111'     ;191
;#DEFINE     LOWER_POS_0         B'10110001'     ;177
;#DEFINE     LOWER_POS_1         B'10110100'     ;180
;#DEFINE     LOWER_POS_2         B'10110111'     ;183
;#DEFINE     LOWER_POS_3         B'10111010'     ;186
;#DEFINE     LOWER_POS_4         B'10111110'     ;190
;#DEFINE     LOWER_POS_5         B'11000001'     ;192
;***************

TEMP_LCD    EQU     0x20        ;BUFFER FOR LCD INSTRUCTION
DAT         EQU     0X21        ;BUFFER FOR LCD DATA
DELAY1      EQU     0X25
DELAY2      EQU     0X26
TEMP_CHAR   EQU     0X27        ;BUFFER FOR KEY PAD CHAR

TEMP_VAR    EQU     0X28        ;BUFFER
TEMP_VAR2   EQU     0X29        ;BUFFER
NUM_BOX0    EQU     0X2A        ;STORE UNIT DIGIT OF NUMBER OF BOXES, BIT0:3 FOR NUMBER, BIT4 FOR SET/NOT SET(1/0)
NUM_BOX1    EQU     0X2B        ;STORE TENTH DIGIT OF NUMBER OF BOXES
NUM_BOX     EQU     0X2C        ;STORE NUMBER OF BOXES(NUM_BOX1*10+NUM_BOX)
TEMP_VAR3   EQU     0X2D        ;BUFFER
TEMP_VAR4   EQU     0X2E        ;BUFFER

BOX_COUNTER     EQU     0X2F    ;COUNT THE BOX
BALL_COUNTER    EQU     0X30    ;COUNT THE BALL (VALUE FROM 1 -6)
CURRENT_COLOR   EQU     0X31    ;CURRENT COLOR OF THE BALL TO DISPENSE (0/1)
CURRENT_PATTERN EQU     0X32



W_RESERV_COUNTER    EQU 0X33    ;COUNT THE NUMBER OF WHITE BALLS IN RESERVOIR
R_RESERV_COUNTER    EQU 0X34    ;COUNT THE NUMBER OF RED BALLS IN RESERVOIR

BALL_PATTERN_MASK   EQU 0X35    ;BIT MASK FOR PATTERN

SERVO_PERIOD_COUNT  EQU 0X36    ;COUNT FROM 0 TO 200 FOR 20MS
UPPER_SERVO_MARK    EQU 0X37    ;MARK DUTY CYCLE LENGTH = (200-THIS)*0.1ms, [180-190]
LOWER_SERVO_MARK    EQU 0X38

WREG_TEMP           EQU 0X39    ;STACK REGS FOR INTERRUPT(LOW)
STATUS_TEMP         EQU 0X3A
BSR_TEMP            EQU 0X3B

PATTERN_STACK   EQU     0X100   ;THE PATTERN STACK STARTS AT 0X00 OF "BANK1"

code
	global TEST_SERVO

TEST_SERVO
        CALL            POLL_KEY
        MOVWF           TEMP_VAR2        ;STORE KEY HEX IN TEMP_VAR2
SERVO_CHECK_KEY2
        MOVLW           KEY2
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY1
        BRA             SERVO_CHECK_KEY3 ; NO
        CALL            START_BELT_DC        ; YES
        BRA             TEST_SERVO
SERVO_CHECK_KEY3
        MOVLW           KEY3
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY2
        BRA             SERVO_CHECK_KEY4 ; NO
        CALL            STOP_BELT_DC        ; YES
        BRA             TEST_SERVO
SERVO_CHECK_KEY4
        MOVLW           KEY4
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY1
        BRA             SERVO_CHECK_KEY5 ; NO
        MOVLW           LOWER_POS_0        ; YES
        MOVWF           LOWER_SERVO_MARK
        BRA             TEST_SERVO
SERVO_CHECK_KEY5
        MOVLW           KEY5
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY2
        BRA             SERVO_CHECK_KEY6 ; NO
        MOVLW           LOWER_POS_1        ; YES
        MOVWF           LOWER_SERVO_MARK
        BRA             TEST_SERVO
SERVO_CHECK_KEY6
        MOVLW           KEY6
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY3
        BRA             SERVO_CHECK_KEY7 ; NO
        MOVLW           LOWER_POS_2        ; YES
        MOVWF           LOWER_SERVO_MARK
        BRA             TEST_SERVO
SERVO_CHECK_KEY7
        MOVLW           KEY7
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY4
        BRA             SERVO_CHECK_KEY8 ; NO
        MOVLW           LOWER_POS_3        ; YES
        MOVWF           LOWER_SERVO_MARK
        BRA             TEST_SERVO
SERVO_CHECK_KEY8
        MOVLW           KEY8
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY5
        BRA             SERVO_CHECK_KEY9 ; NO
        MOVLW           LOWER_POS_4        ; YES
        MOVWF           LOWER_SERVO_MARK
        BRA             TEST_SERVO
SERVO_CHECK_KEY9
        MOVLW           KEY9
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY6
        BRA             SERVO_CHECK_KEYS ; NO
        MOVLW           LOWER_POS_5        ; YES
        MOVWF           LOWER_SERVO_MARK
        BRA             TEST_SERVO
SERVO_CHECK_KEYS
        MOVLW           KEYS
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY7
        BRA             SERVO_CHECK_KEY0 ; NO
        MOVLW           UPPER_POS_WHITE        ; YES
        MOVWF           UPPER_SERVO_MARK
        BRA             TEST_SERVO
SERVO_CHECK_KEY0
        MOVLW           KEY0
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY5
        BRA             SERVO_CHECK_KEYP ; NO
        MOVLW           UPPER_POS_CENTER        ; YES
        MOVWF           UPPER_SERVO_MARK
        BRA             TEST_SERVO
SERVO_CHECK_KEYP
        MOVLW           KEYP
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY6
        BRA             SERVO_CHECK_KEYA             ; NO
        MOVLW           UPPER_POS_RED        ; YES
        MOVWF           UPPER_SERVO_MARK
        BRA             TEST_SERVO
SERVO_CHECK_KEYA
        MOVLW           KEYA
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY6
        BRA             SERVO_CHECK_KEYB             ; NO
        CALL            LOWER_LATCH_MOTOR        ; YES
        BRA             TEST_SERVO
SERVO_CHECK_KEYB
        MOVLW           KEYB
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY6
        BRA             SERVO_CHECK_KEYC        ; NO
        CALL            LIFT_LATCH_MOTOR        ; YES
        BRA             TEST_SERVO
SERVO_CHECK_KEYC
        MOVLW           KEYC
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY6
        BRA             SERVO_CHECK_KEYD             ; NO
        CALL            STOP_LATCH_MOTOR        ; YES
        BRA             TEST_SERVO
SERVO_CHECK_KEYD
        MOVLW           KEYD
        CPFSEQ          TEMP_VAR2, W     ; CHECK IF IS KEY6
        BRA             TEST_SERVO             ; NO
        RETURN
END